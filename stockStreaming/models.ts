export interface User {
    id: number,
    password: string,
    username: string,
    adimin?: boolean,
}

export interface DayData {
    id: number;
    stocks_name: string;
    Date: Date;
    Open: number;
    High: number;
    Low: number;
    Close: number;
    "Adj Close": number;
    Volume: number;
}

export interface YearData {
    id: number,
    stocks_name: string,
    date: Date,
    returnOnAssets?: number;
    ROA?:number;
    returnOnEquity?: number;
    ROE?:number;
    grossProfitMargin: number,
    netProfitMargin: number,
    priceBookValueRatio?: number;
    PB_ratio?:number;
    priceEarningsRatio?: number;
    PE_ratio?:number;
    currentRatio?:number;
    dividendYield?:number;
    debtRatio?: number;
    debtEquityRatio?: number;
    returnOnCapitalEmployed?: number;
    operatingCashFlowPerShare?: number;
    freeCashFlowPerShare?: number;
    priceEarningsToGrowthRatio?: number;
}

export interface FavStock {
    id: number,
    users_id: number,
    stocks_name: string,
}

export interface Stock {
    id: number,
    name?: string,
    shortName?: string;
    num: string,
    logo?: string
    logo_url?: string;
}