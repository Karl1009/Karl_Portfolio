# if you want to download data, please pip install
# pandas
# yfinance
# YahooFinancials

import pandas as pd
import yfinance as yf

import time
import datetime
from datetime import date, timedelta

today = date.today()
last_year = today - timedelta(days=365)

stocks = ["AAPL","MSFT","AMZN","FB","GOOG","TSLA","NVDA","PYPL","ADBE","NFLX","INTC","CMCSA","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","MDLZ","ISRG","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","MNST","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","XEL","EXC","WBA","DOCU","CTSH","ORLY","NXPI","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS","ASML","FAST","SGEN","MRNA","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","ALGN","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CHKP","CTXS","CDW","TCOM","ULTA","EXPE","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]
for i in range(len(stocks)):
    time.sleep(1)
    stock_df = yf.download(stocks[i], 
                      start=last_year, 
                      end=today, 
                      progress=False)
    stock_df.head()
    root = "stock_Data"
    stock_df.to_csv(root +'/' + "{}_data.csv".format(stocks[i]))
    print("No.{} {} data has been downloaded, only {} left, please wait".format(i +1 , stocks[i], len(stocks)- i))
    time.sleep(1)
    

