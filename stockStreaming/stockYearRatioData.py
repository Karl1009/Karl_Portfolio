import json 
import csv 
import requests


stocks = ["AAPL","MSFT","AMZN","FB","GOOG","TSLA","NVDA","PYPL","ADBE","NFLX","INTC","CMCSA","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","MDLZ","ISRG","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","MNST","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","XEL","EXC","WBA","DOCU","CTSH","ORLY","NXPI","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS","ASML","FAST","SGEN","MRNA","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","ALGN","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CHKP","CTXS","CDW","TCOM","ULTA","EXPE","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]

for i in range(len(stocks)):
    url = "https://financialmodelingprep.com/api/v3/ratios/{}?apikey=7f52bd4bf87ccff03b2eb7752cd0d21b".format(stocks[i])
    r = requests.get(url)
    print(type(r))
    data = r.json()

    data_file = open('stock_Year_Data/{}_data.csv'.format(stocks[i]), 'w') 
    
    csv_writer = csv.writer(data_file) 
    
    count = 0
    
    for key in data: 
        if count == 0: 
            header = key.keys() 
            csv_writer.writerow(header) 
            count += 1
    

        csv_writer.writerow(key.values()) 
    
    data_file.close() 
