import Knex from 'knex';
import { User } from '../models';

export class UserService {
    constructor(private knex: Knex) { }

    async login(email: string): Promise<User[]> {
        const result = await this.knex("users").where({ username: email });
        const users: User[] = result;
        return users;
    }

    async register(email: string, password: string,): Promise<number[]> {
        const result = await this.knex("users").insert({
            username: email,
            password: password,
            admin: false
        }).returning('id');         
        return result;
       
    }


}