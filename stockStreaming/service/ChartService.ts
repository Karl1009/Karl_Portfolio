import Knex from 'knex';
import jsonfile from 'jsonfile';
import path from 'path';


export class ChartService {
    constructor(private knex: Knex) {}
    
    async take(stockID: string): Promise<number[]> {
      const x = await this.knex("fav_stock")
      console.log(x)
        const result_JSON = path.join(__dirname,`../stock_Data_json/${stockID}.json`);
        const result:any = await jsonfile.readFile(result_JSON);
        //await this.knex("fav_stock").insert({
        //    users_id: uesrID,
        //    stocks_name: stockName,
        //}).returning('id')
        return result;
    }
  }