import Knex from 'knex';
import { FavStock, YearData } from '../models';
import moment from 'moment';



export class FavService {
    constructor(private knex: Knex) { }

    async add(uesrID: number, stockNum: string): Promise<number[]> {
        const check = await this.knex("fav_stock")
            .where('users_id', uesrID)
            .andWhere('stocks_num', stockNum)
            .returning('id');
        if (check[0]) {
            return [0]
        } else {
           const result = await this.knex("fav_stock").insert({
                users_id: uesrID,
                stocks_num: stockNum,
            }).returning('id')        
        return result;
        }
            
    };


    async remove(uesrID: number, stockNum: string) {
        console.log(uesrID)
        console.log(stockNum)
        const result = await this.knex("fav_stock")
            .where('users_id', uesrID)
            .andWhere('stocks_num', stockNum)
            .returning('id');
        const FavStock = result[0];    
        const id = FavStock.id;
        const count = await this.knex("fav_stock").where({
            id: id
        }).del();
        return count;
    }

    async check(uesrID: number, stockNum: string): Promise<boolean> {
        const result = await this.knex("fav_stock")
            .where('users_id', uesrID)
            .andWhere('stocks_num', stockNum)
            .returning('id');
        if (result[0]) {
            return true
        } else {
            return false
        }
    }

    async favList(userID: number): Promise<FavStock[]> {
        const result = await this.knex("fav_stock").select('stocks_num').where('users_id', userID);
        return result;
    }

    async dayClose(stockNum: string) {
        const lastWeek = moment().subtract(7, 'days').format('YYYY-MM-DD');
        const weekData = await this.knex("day_price").select('Date', 'Close')
            .where('stocks_num', stockNum).andWhere('Date', '>', lastWeek);
        const dataLength = weekData.length;
        const today = dataLength - 1;
        const yesterday = dataLength - 2;
        const todayClose = weekData[today].Close;
        const yesterdayClose = weekData[yesterday].Close
        console.log(`today:${weekData[today].Date}, yesterday:${weekData[yesterday].Date}`)
        const result = (todayClose - yesterdayClose) / yesterdayClose
        return result * 100;
    }

    async yearData(stockNum: string): Promise<YearData[]> {
        const lastYear = moment().subtract(1, 'years').format('YYYY-MM-DD');
        const thisYear = await this.knex("year_data")
            .select('ROA', 'ROE', 'grossProfitMargin', 'netProfitMargin', 'PB_ratio', 'PE_ratio')
            .where('Date', '>', lastYear)
            .andWhere('stocks_num', stockNum);
        return thisYear
    };
};