import Knex from 'knex';
import { YearData, Stock } from '../models';


export class StockService {
    constructor(private knex: Knex) { }

    async stocksList(): Promise<Stock[]> {
        const stocks = await this.knex("stocks").select('name', 'num');
        
        return stocks
    } 

    async stock(stockNum: string): Promise<Stock[]> {
        const stock = await this.knex("stocks")
        .select('name', 'num', 'logo').where('num', stockNum);
        console.log(stock)
        return stock
    };
    
    async year_data(stockNum: string): Promise<YearData[]> {
        const stock = await this.knex("year_data").where('stocks_num', stockNum);
        return stock
    };
};