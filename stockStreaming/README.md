
## Stock data streaming platform

AiStock aims to provide a platform for investors who want annual data for their long term investment strategy. We also provide a daily chart for investors to 
monitor their portfolio. 

## Functions:

1. Our platform supports up to 30 years of stock annual data for each stock of NASDAQ/DOW, which is very rare and treasurable data on the internet today. 

2. We also use API of yahoo finance for precise daily stock price records for users to monitor their stock investment position. We use Python Pandas for the transformation of the dataframe data to CSV file and then to the JSON file, which eventually be loaded on CASVAS.js. 

3. The platform also supports a portfolio page for users to plan their stock investment strategy.

4. AiStock also wants to give advice to investors for better stock options by backend AI calculation. (In development)
