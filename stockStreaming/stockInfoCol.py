from csv import reader, writer 

stocks = ["AAPL","MSFT","AMZN","FB","GOOG","TSLA","NVDA","PYPL","ADBE","NFLX","INTC","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","EXC","WBA","DOCU","CTSH","ORLY","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS", "FAST","SGEN","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CTXS","CDW","ULTA","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]
#"CMCSA" "MDLZ" "ISRG" "MNST" "XEL", "NXPI" "ASML" "MRNA","ALGN" "CHKP" "EXPE""TCOM"list index out of range
for i in range(len(stocks)):
  with open('./stock_Info/{}.csv'.format(stocks[i])) as f, open('./stock_Info_Col/{}.csv'.format(stocks[i]), 'w') as fw: 
    writer(fw, delimiter=',').writerows(zip(*reader(f, delimiter=',')))