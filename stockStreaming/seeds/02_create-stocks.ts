import * as Knex from "knex";
import xlsx from 'xlsx';
import { Stock } from "../models";

const stockTable = 'stocks'

export async function seed(knex: Knex): Promise<void> {

  await knex(stockTable).del();

  let stocks = ["AAPL","MSFT","AMZN","FB","GOOG"]
  //,"TSLA","NVDA","PYPL","ADBE","NFLX","INTC","CMCSA","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","MDLZ","ISRG","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","MNST","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","XEL","EXC","WBA","DOCU","CTSH","ORLY","NXPI","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS","ASML","FAST","SGEN","MRNA","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","ALGN","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CHKP","CTXS","CDW","TCOM","ULTA","EXPE","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]
  
  for(let i =0 ; i < stocks.length; i ++){
    const workbook = xlsx.readFile(`./stock_Info_Col/${stocks[i]}.csv`)
    const infos:Stock[] = xlsx.utils.sheet_to_json(workbook.Sheets["Sheet1"], {raw: false});
    if(workbook)
   
    for(let info of infos){
        await knex(stockTable).insert([
            {   name: info.shortName,
                num: stocks[i],
                logo: info.logo_url
            }]);
        console.log(`Table stocks${stocks[i]} is completed`);
    }    
  };
}
