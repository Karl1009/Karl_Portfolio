import * as Knex from "knex";

// import moment from 'moment';

const userTable= 'users'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex(userTable).del();
  
    // Inserts seed entries
    await knex(userTable).insert([
        { id: 1, username:'admin01@admin.com',password:'admin',admin:'true'},
        { id: 2, username:'user01@user.com',password:'user',admin:'false'},
    ]);
};
