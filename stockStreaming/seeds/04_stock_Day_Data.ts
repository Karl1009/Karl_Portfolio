import * as Knex from "knex";
import xlsx from 'xlsx';
import { DayData } from "../models";

const dayTable = 'day_price'

export async function seed(knex: Knex): Promise<void> {

    await knex(dayTable).del();

    let stocks = ["AAPL", "MSFT", "AMZN", "FB", "GOOG"]
    //,"TSLA","NVDA","PYPL","ADBE","NFLX","INTC","CMCSA","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","MDLZ","ISRG","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","MNST","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","XEL","EXC","WBA","DOCU","CTSH","ORLY","NXPI","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS","ASML","FAST","SGEN","MRNA","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","ALGN","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CHKP","CTXS","CDW","TCOM","ULTA","EXPE","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]
    for (let i = 0; i < stocks.length; i++) {
        const workbook = xlsx.readFile(`./stock_Data/${stocks[i]}_data.csv`)
        const days: DayData[] = xlsx.utils.sheet_to_json(workbook.Sheets["Sheet1"], { raw: false });
        for (let day of days) {
            await knex(dayTable).insert([
                {
                    stocks_num: stocks[i],
                    Date: day.Date,
                    Open: day.Open,
                    High: day.High,
                    Low: day.Low,
                    Close: day.Close,
                    Adj_Close: day["Adj Close"],
                    Volume: day.Volume
                },
            ]);
        }
        console.log(`Table day_price ${stocks[i]} is completed`);
    }
};
