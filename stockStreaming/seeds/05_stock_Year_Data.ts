import * as Knex from "knex";
import xlsx from 'xlsx';
import { YearData } from "../models";

const YearTable = 'year_data'

export async function seed(knex: Knex): Promise<void> {

  await knex(YearTable).del();

  let stocks = ["AAPL", "MSFT", "AMZN", "FB", "GOOG"]
  //,"TSLA","NVDA","PYPL","ADBE","NFLX","INTC","CMCSA","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","MDLZ","ISRG","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","MNST","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","XEL","EXC","WBA","DOCU","CTSH","ORLY","NXPI","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS","ASML","FAST","SGEN","MRNA","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","ALGN","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CHKP","CTXS","CDW","TCOM","ULTA","EXPE","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]

  for (let i = 0; i < stocks.length; i++) {
    const workbook = xlsx.readFile(`./stock_Year_Data/${stocks[i]}_data.csv`)
    const years: YearData[]= xlsx.utils.sheet_to_json(workbook.Sheets["Sheet1"], { raw: false });

    for (let year of years) {
      await knex(YearTable).insert([
        {
          stocks_num: stocks[i],
          Date: year.date,
          ROA: year.returnOnAssets,
          ROE: year.returnOnEquity,
          grossProfitMargin: year.grossProfitMargin,
          netProfitMargin: year.netProfitMargin,
          PB_ratio: year.priceBookValueRatio,
          PE_ratio: year.priceEarningsRatio,
          dividendYield: year.dividendYield,
          currentRatio: year.currentRatio,
          debtRatio: year.debtRatio,
          debtEquityRatio: year.debtEquityRatio,
          returnOnCapitalEmployed: year.returnOnCapitalEmployed,
          operatingCashFlowPerShare: year.operatingCashFlowPerShare,
          freeCashFlowPerShare: year.freeCashFlowPerShare,
          priceEarningsToGrowthRatio: year.priceEarningsToGrowthRatio
        }
      ]);
    }
    console.log(`Table year_price ${stocks[i]} is completed`);
  }
};
// table.float('dividendYield');
//         table.float('currentRatio').notNullable()
//         table.float('debtRatio').notNullable();
//         table.float('debtEquityRatio').notNullable();
//         table.float('returnOnCapitalEmployed').notNullable() // = ROC
//         table.float('operatingCashFlowPerShare').notNullable();
//         table.float('freeCashFlowPerShare').notNullable();
//         table.float('priceEarningsToGrowthRatio').notNullable();