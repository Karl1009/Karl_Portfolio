import * as Knex from "knex";

 const favTable= 'fav_stock'

export async function seed(knex: Knex): Promise<void> {
    //Deletes ALL existing entries
    await knex(favTable).del();
  
    // Inserts seed entries
    await knex(favTable).insert([
        { users_id:'2',stocks_num:'AAPL'},
        { users_id:'2',stocks_num:'MSFT'},
        { users_id:'2',stocks_num:'AMZN'},
        { users_id:'2',stocks_num:'GOOG'},
    ]);
};
