import * as Knex from "knex";

const favTable = 'fav_stock'
export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(favTable)) {
        return
    }
    await knex.schema.createTable(favTable, table => {
        table.increments();
        table.integer("users_id").unsigned();
        table.foreign('users_id').references('users.id');
        table.string("stocks_num").unsigned();
        table.foreign('stocks_num').references('stocks.num');
        table.timestamps(false, true);
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(favTable);
}


