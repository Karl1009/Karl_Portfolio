import * as Knex from "knex";

const stockTable = 'stocks'
export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(stockTable)) {
        return
      }
    await knex.schema.createTable(stockTable, table => {
        table.increments();
        table.string('name').notNullable().unique();
        table.string('num').notNullable().unique();
        table.string('logo')
        table.timestamps(false, true);
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(stockTable);
}

