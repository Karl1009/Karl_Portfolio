import * as Knex from "knex";

const dayTable = 'day_price'

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(dayTable)) {
        return
      }
    await knex.schema.createTable(dayTable, table => {
        table.increments();
        table.string("stocks_num").unsigned();
        table.foreign('stocks_num').references('stocks.num');
        table.date('Date').notNullable();
        table.float('Open').notNullable();
        table.float('High').notNullable();
        table.float('Low').notNullable();
        table.float('Close').notNullable();//
        table.float('Adj_Close').notNullable();
        table.integer('Volume').notNullable();
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(dayTable);
}
