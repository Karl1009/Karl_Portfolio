import * as Knex from "knex";

const yearDataTable = 'year_data'

export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable(yearDataTable)) {
        return
      }
    await knex.schema.createTable(yearDataTable, table => {
        table.increments();
        table.string("stocks_num").unsigned();
        table.foreign('stocks_num').references('stocks.num');
        table.date('Date').notNullable();
        table.float('ROA').notNullable();
        table.float('ROE').notNullable();
        table.float('grossProfitMargin').notNullable();
        table.float('netProfitMargin').notNullable();
        table.float('PB_ratio').notNullable();
        table.float('PE_ratio').notNullable();
        // 以下是新增data
        table.float('dividendYield');
        table.float('currentRatio')
        table.float('debtRatio')
        table.float('debtEquityRatio')
        table.float('returnOnCapitalEmployed') // = ROC
        table.float('operatingCashFlowPerShare')
        table.float('freeCashFlowPerShare')
        table.float('priceEarningsToGrowthRatio')

    });
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(yearDataTable);
}