import csv
import json

stocks = ["AAPL","MSFT","AMZN","FB","GOOG","TSLA","NVDA","PYPL","ADBE","NFLX","INTC","CMCSA","PEP","CSCO","COST","TMUS","AMGN","AVGO","QCOM","TXN","CHTR","AMD","SBUX","GILD","MDLZ","ISRG","INTU","BKNG","VRTX","FISV","AMAT","ATVI","REGN","ADP","JD","MELI","CSX","LRCX","ADSK","ILMN","MU","BIIB","MNST","KHC","LULU","ADI","ZM","EA","EBAY","DXCM","XEL","EXC","WBA","DOCU","CTSH","ORLY","NXPI","NTES","ROST","CTAS","KLAC","IDXX","BIDU","WDAY","MAR","PCAR","VRSK","SPLK","CDNS","SNPS","ASML","FAST","SGEN","MRNA","ANSS","PAYX","SIRI","XLNX","MCHP","SWKS","ALGN","VRSN","CPRT","DLTR","ALXN","CERN","BMRN","INCY","TTWO","MXIM","CHKP","CTXS","CDW","TCOM","ULTA","EXPE","WDC","NTAP","LBTYK","FOXA","FOX","LBTYA"]

for i in range(len(stocks)):
  csvfile = open('./stock_Data/{}_data.csv'.format(stocks[i]), 'r')
  jsonfile = open('./public/frontend_stockdata_json/{}_data.json'.format(stocks[i]), 'w')

  fieldnames = ("Date","Open","High","Low", "Close", "Adj Close", "Volume")
  reader = csv.DictReader( csvfile, fieldnames)
  jsonfile.write('[')
  header = 0
  for row in reader:
    if header > 0:
      json.dump(row, jsonfile)
      jsonfile.write(',\n')
    header = header + 1
  jsonfile.write('{}]')

