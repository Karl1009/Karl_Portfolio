const exit = document.getElementById('exit');
const loginPanelBtn = document.getElementById('login-panel-btn');
const panel = document.querySelector('.panel');
const loginForm = document.getElementById('login-form');
const regForm = document.getElementById('reg-form');
const logoutBtn = document.getElementById('logout-btn')
const logoutText = document.querySelector('.logout-text')


exit.addEventListener('click', function () {
    panel.style.display = 'none'
});

loginPanelBtn.addEventListener('click', function () {
    panel.style.display = 'flex'
})

regForm.addEventListener('submit', async function (event) {
    event.preventDefault();
    const form = this;

    if (form.password.value != form.password2.value) {
        alert('請輸入相同密碼');
        return;
    };

    const formObject = {};
    formObject['username'] = form.email.value;
    formObject['password'] = form.password.value;

    const res = await fetch('/register', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObject)
    });

    const result = await res.json();

    if (result.success) {
        alert('User Registered');
        await checkLogin();
        panel.style.display = 'none';
    } else if (!result.success) {
        alert('User already exist');
    }
})

loginForm.addEventListener('submit', async function (event) {
    event.preventDefault();
    const form = event.target;
    const formObject = {};
    formObject['username'] = form.email.value;
    formObject['password'] = form.password.value;

    const res = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(formObject)
    });

    const result = await res.json();
    if (res.status === 200 && result.success) {
        await checkLogin()
        panel.style.display = 'none';
    } else {
        alert(`Please try again`);
    }
})

async function checkLogin() {
    const res = await fetch('/loginCheck');
    const user = await res.json();
    if (user.success) {
        loginPanelBtn.classList.add('d-none');
        logoutBtn.classList.remove('d-none');        
    }
    if (res.status == 401) {
        logoutBtn.classList.add('d-none');
        loginPanelBtn.classList.remove('d-none');     
    }
};
checkLogin();

logoutBtn.addEventListener('click', async function (event) {
    const res = await fetch('/logout', {
        method: "GET"
    });
    const result = await res.json();
    await checkLogin();
})

logoutBtn.addEventListener('mouseover', async function (event) {
    logoutText.classList.remove('d-none');
})
logoutBtn.addEventListener('mouseout', async function (event) {
    logoutText.classList.add('d-none');
})

