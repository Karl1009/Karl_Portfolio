

window.onload = function () {
    var dps1 = [], dps2 = [];
    var stockChart = new CanvasJS.StockChart("chartContainer", {
        theme: "light2",
        exportEnabled: true,
        title: {
            text: "AAPL StockChart"
        },
        subtitles: [{
            text: "(in USD)"
        }],
        charts: [{
            axisX: {
                crosshair: {
                    enabled: true,
                    snapToDataPoint: true
                }
            },
            axisY: {
                prefix: "$"
            },
            data: [{
                type: "candlestick",
                yValueFormatString: "$#,###.##",
                dataPoints: dps1
            }]
        }],
        navigator: {
            data: [{
                dataPoints: dps2
            }],
            slider: {
                minimum: new Date(2019, 08, 20),
                maximum: new Date(2019, 12, 31)
            }
        }
    });
    $.getJSON("/frontend_stockdata_json/AAPL_data.json", function (data) {
        for (var i = 0; i < data.length; i++) {
            dps1.push({ x: new Date(data[i].Date), y: [Number(data[i].Open), Number(data[i].High), Number(data[i].Low), Number(data[i].Close)] });
            dps2.push({ x: new Date(data[i].Date), y: Number(data[i].Close) });
        }
        stockChart.render();
    });
}