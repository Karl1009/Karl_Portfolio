

const stocksForm = document.getElementById('stocks-form');
const stocksOption = document.getElementById('stocks-option');

const stockHead = document.getElementById('stock-title');

const yearList = document.getElementById('stock-year-list')


async function loadStocksOption() {
    const res = await fetch('/stock/stocksList');
    const stocks = await res.json();
    stocksOption.innerHTML = "<option selected>Choose...</option>";

    for (let stock of stocks) {
        stocksOption.innerHTML += `<option value="${stock.num}">
                                    <span>${stock.num}       </span><span>${stock.name} </span>
                                    </option>`;
    };
};
loadStocksOption()

stocksForm.addEventListener('submit', async function (event) {
    event.preventDefault();
    const form = event.target;
    const formData = new FormData();
    const stock = form.stock.value;
    if (form.stock.value === 'Choose...') {
        return;
    }
    const res1 = await fetch(`/stock/${stock}`)
    const title = await res1.json();
    const data = title[0]

    let img = '';
    if (data.logo) {
        img = `<img src="${data.logo}"/>`
    } else { img = `<i class="far fa-folder-open"></i>` }

    stockHead.innerHTML = ` <div>${img}</div>
                            <div id ='stock-num' >${data.num}</div>
                            <div>${data.name}</div>
                            <div class='add-fav-panel d-none col-1 '>
                                <div class="d-none" id='remove-fav'><i class="fas fa-bookmark"></i></div>
                                <div id='add-fav'><i class="far fa-bookmark"></i></div>
                            </div>
                            `

    const addFavPanel = document.querySelector('.add-fav-panel');
    const resLogin = await fetch('/loginCheck');
    const user = await resLogin.json();
    if (user.success) {
        addFavPanel.classList.remove('d-none')
    }
    if (user.status == 401) {        
        addFavPanel.classList.add('d-none')
    }

    const removeBtn = document.getElementById('remove-fav');
    const addBtn = document.getElementById('add-fav');

    // async function check(stock) {
    const resCheck = await fetch(`/fav/check/${data.num}`);
    const result = await resCheck.json();
    if (result.success) {
        addBtn.classList.remove('d-none');
        removeBtn.classList.add('d-none');
    };
    if (result.message == "Already Bookmarked") {
        addBtn.classList.add('d-none');
        removeBtn.classList.remove('d-none');
    };
    //     };
    // };
    // check(data.num);

    addBtn.addEventListener('click', async function () {
        const resAdd = await fetch(`/fav/add/${data.num}`, {
            method: "POST",
        });
        const result = await resAdd.json();
        if (result.success) {
            addBtn.classList.add('d-none');
            removeBtn.classList.remove('d-none');
        };
        if (result.message == "Already Delete") {
            addBtn.classList.remove('d-none');
            removeBtn.classList.add('d-none');
        };
    });

    removeBtn.addEventListener('click', async function () {
        const resDel = await fetch(`/fav/remove/${data.num}`, {
            method: "DELETE",
        });
        const result = await resDel.json();
        if (result.success) {
            addBtn.classList.remove('d-none');
            removeBtn.classList.add('d-none');
        };
        if (result.message == "Already Bookmarked") {
            addBtn.classList.add('d-none');
            removeBtn.classList.remove('d-none');
            // console.log(result);
            // check(data.num)
        };
    });
    // add(data.num);
    // remove(data.num)
    // year list area
    const res2 = await fetch(`/stock/year/${stock}`);
    const years = await res2.json();

    for (let year of years) {
        const date = year.Date.substr(0, 10);
        const PB_ratio = parseFloat(year.PB_ratio).toFixed(2);
        const PE_ratio = parseFloat(year.PE_ratio).toFixed(2);
        const ROA = parseFloat(year.ROA * 100).toFixed(2);
        const ROE = parseFloat(year.ROE * 100).toFixed(2);
        const currentRatio = parseFloat(year.currentRatio).toFixed(2)
        const debtEquityRatio = parseFloat(year.debtEquityRatio).toFixed(2);
        const debtRatio = parseFloat(year.debtRatio).toFixed(2);;

        let yield = ''
        if (isNaN(year.dividendYield)) {
            yield = '/'
        } else { yield = parseFloat(year.dividendYield * 100).toFixed(2); }

        const freeCashFlowPerShare = parseFloat(year.freeCashFlowPerShare).toFixed(2);
        const grossProfitMargin = parseFloat(year.grossProfitMargin * 100).toFixed(2);
        const netProfitMargin = parseFloat(year.netProfitMargin * 100).toFixed(2);
        const operatingCashFlowPerShare = parseFloat(year.operatingCashFlowPerShare).toFixed(2);
        const priceEarningsToGrowthRatio = parseFloat(year.priceEarningsToGrowthRatio).toFixed(2);
        const ROC = parseFloat(year.returnOnCapitalEmployed * 100).toFixed(2);

        yearList.innerHTML += `<tr>
                                <th scope="row">${date}</th>
                                <td>${ROA}</td>
                                <td>${ROE}</td>
                                <td>${ROC}</td>
                                <td>${grossProfitMargin}</td>
                                <td>${netProfitMargin}</td>
                                <td>${PB_ratio}</td>
                                <td>${PE_ratio}</td>
                                <td>${yield}</td>
                                <td>${currentRatio}</td>
                                <td>${debtRatio}</td>
                                <td>${debtEquityRatio}</td>
                                <td>${priceEarningsToGrowthRatio}</td>
                                <td>${freeCashFlowPerShare}</td>
                            </tr> `
    };
})




// for pages
const yearPanelBtn = document.getElementById('year-panel-btn')
const favPanelBtn = document.getElementById('fav-panel-btn');
//section
const stockData = document.getElementById('stock-data');
const favListPage = document.getElementById('fav-list-page');
//fav-list-area
const title = document.getElementById('fav-list-title');
const listData = document.getElementById('fav-stock-list');

yearPanelBtn.addEventListener('click', async function (event) {
    stockData.classList.remove('d-none');

    //orther section
    favListPage.classList.add('d-none');

})

favPanelBtn.addEventListener('click', async function (event) {
    const res = await fetch('/loginCheck');
    const user = await res.json();
    if (user.success) {
        stockData.classList.add('d-none');
        //fav-list-area
        favListPage.classList.remove('d-none');
        await getFavList();
    };
    if (res.status == 401) {
        panel.style.display = 'flex';
    };
});



async function getFavList() {
    const res1 = await fetch('/fav/list');
    const stockNums = await res1.json();
    if (res1.status === 503) {
        title.innerHTML = "沒有任何訂閱";
    };
    if (stockNums) {
        listData.innerHTML = '';
        for (let i in stockNums) {
            const id = stockNums[i].stocks_num;
            const res2 = await fetch(`/fav/day/${id}`);
            const res3 = await fetch(`/fav/year/${id}`);
            const day = await res2.json();
            const dayClose = parseFloat(day).toFixed(2)
            const yearData = await res3.json();
            const year = yearData[0]
            let x = '';
            if (dayClose >= 0) {
                x = '<div class="green-tri">▲</div>'
            } else {
                x = '<div class="red-tri">▼</div>'
            };
            listData.innerHTML += ` <tr >
                                        <th scope="row">${id}</th>
                                        <td><div>${dayClose}%</div>${x}</td>
                                        <td></td>
                                        <td>${year.ROA}</td>
                                        <td>${year.ROE}</td>
                                        <td>${year.grossProfitMargin}</td>
                                        <td>${year.netProfitMargin}</td>
                                        <td>${year.PB_ratio}</td>                                      
                                        <td>${year.PE_ratio}</td>
                                        <td class='del-btn' data-id='${id}'><i class="fas fa-trash"></i></td>
                                    </tr>`
        }
    }
}

const delStocks = Array.from(document.querySelectorAll('.del-btn'));
for (let delStock of delStocks) {
    delStock.addEventListener('click', async function () {
        const id = delStock.getAttribute('data-id');
        const res = await fetch(`/fav/remove/${id}`, {
            method: "DELETE",
        });
        const result = await res.json();
        console.log(result);
        await getFavList();
    });
};



