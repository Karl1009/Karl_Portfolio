const favSubBtn = document.getElementById('fav-sub-btn');

var savedStockName = ""

search.addEventListener('submit', async function (event) {
    event.preventDefault();
    const form = event.target;
    const name = form.stockname.value
    const stockname = name.toUpperCase();
    changestock(stockname)
    savedStockName = stockname;
    document.querySelector('#searchChart').classList.remove('d-none');
    document.querySelector('#intro').classList.add('d-none');
    document.querySelector('#fav-list-page').classList.add('d-none');
});

favSubBtn.addEventListener('click', async function () {
    let id = '';
    id = savedStockName;
    // const id = delStock.getAttribute('data-id');
    const res = await fetch(`/fav/add/${id}`, {
        method: "POST",
    });
    const result = await res.json();
    console.log(result);
    await getFavList();
}); 

function changestock(stockname) {
    var dps1 = [], dps2 = [];
    var stockChart = new CanvasJS.StockChart("chartContainer", {
        theme: "light2",
        exportEnabled: true,
        title: {
            text: `${stockname} StockChart`
        },
        subtitles: [{
            text: "(in USD)"
        }],
        charts: [{
            axisX: {
                crosshair: {
                    enabled: true,
                    snapToDataPoint: true
                }
            },
            axisY: {
                prefix: "$"
            },
            data: [{
                type: "candlestick",
                yValueFormatString: "$#,###.##",
                dataPoints: dps1
            }]
        }],
        navigator: {
            data: [{
                dataPoints: dps2
            }],
            slider: {
                minimum: new Date(2019, 08, 20),
                maximum: new Date(2019, 12, 31)
            }
        }
    });
    $.getJSON(`/frontend_stockdata_json/${stockname}_data.json`, function (data) {
        for (var i = 0; i < data.length; i++) {
            dps1.push({ x: new Date(data[i].Date), y: [Number(data[i].Open), Number(data[i].High), Number(data[i].Low), Number(data[i].Close)] });
            dps2.push({ x: new Date(data[i].Date), y: Number(data[i].Close) });
        }
        stockChart.render();
    });
}