const favPanelBtn = document.getElementById('fav-panel-btn');
//section
const intro = document.getElementById('intro');
const favListPage = document.getElementById('fav-list-page');
//fav-list-area
const title = document.getElementById('fav-list-title');
const listData = document.getElementById('fav-stock-list');

// const favSubBtn = document.getElementById('fav-sub-btn');

// var savedStockName = ""



favPanelBtn.addEventListener('click', async function () {
    const res = await fetch('/loginCheck');
    const user = await res.json();

    if (user.success) {
        intro.classList.add('d-none');
        //fav-list-area
        favListPage.classList.remove('d-none');
        await getFavList(); 
        document.querySelector('#searchChart').classList.add('d-none');
    };
    if (res.status == 401) {
        panel.style.display = 'flex';
        
    };
});

async function getFavList() {
    const res1 = await fetch('/fav/list');
    const stockNums = await res1.json();
    if (res1.status === 503) {
        title.innerHTML = "沒有任何訂閱";
    };
    if (stockNums) {
        listData.innerHTML = '';
        for (let i in stockNums) {
            const id = stockNums[i].stocks_num;
            const res2 = await fetch(`/fav/day/${id}`);
            const res3 = await fetch(`/fav/year/${id}`);
            const day = await res2.json();
            const dayClose = parseFloat(day).toFixed(2)
            const yearData = await res3.json();
            const year = yearData[0]
            let x = '';
            if (dayClose >= 0) {
                x = '<div class="green-tri">▲</div>'
            } else {
                x = '<div class="red-tri">▼</div>'
            };
            listData.innerHTML += ` <tr >
                                        <th scope="row">${id}</th>
                                        <td><div>${dayClose}%</div>${x}</td>
                                        <td></td>
                                        <td>${year.ROA}</td>
                                        <td>${year.ROE}</td>
                                        <td>${year.grossProfitMargin}</td>
                                        <td>${year.netProfitMargin}</td>
                                        <td>${year.PB_ratio}</td>                                      
                                        <td>${year.PE_ratio}</td>
                                        <td class='del-btn' data-id='${id}'><i class="fas fa-trash"></i></td>
                                    </tr>`
        }

        const delStocks = Array.from(document.querySelectorAll('.del-btn'));
        for (let delStock of delStocks) {
            delStock.addEventListener('click', async function () {
                const id = delStock.getAttribute('data-id');
                const res = await fetch(`/fav/remove/${id}`, {
                    method: "DELETE",
                });
                const result = await res.json();
                console.log(result);
                await getFavList();
            });
        };

    }

}

// search.addEventListener('submit', async function (event) {
//     event.preventDefault();
//     const form = event.target;
//     const name = form.stockname.value
//     const stockname = name.toUpperCase();
//     changestock(stockname)
//     savedStockName = stockname;
//     document.querySelector('#searchChart').classList.remove('d-none');
//     document.querySelector('#intro').classList.add('d-none');
//     document.querySelector('#fav-list-page').classList.add('d-none');

// });

// favSubBtn.addEventListener('click', async function () {
//     let id = '';
//     id = savedStockName;
//     // const id = delStock.getAttribute('data-id');
//     const res = await fetch(`/fav/add/${id}`, {
//         method: "POST",
//     });
//     const result = await res.json();
//     console.log(result);
//     await getFavList();
// }); 

// function changestock(stockname) {
//     var dps1 = [], dps2 = [];
//     var stockChart = new CanvasJS.StockChart("chartContainer", {
//         theme: "light2",
//         exportEnabled: true,
//         title: {
//             text: `${stockname} StockChart`
//         },
//         subtitles: [{
//             text: "(in USD)"
//         }],
//         charts: [{
//             axisX: {
//                 crosshair: {
//                     enabled: true,
//                     snapToDataPoint: true
//                 }
//             },
//             axisY: {
//                 prefix: "$"
//             },
//             data: [{
//                 type: "candlestick",
//                 yValueFormatString: "$#,###.##",
//                 dataPoints: dps1
//             }]
//         }],
//         navigator: {
//             data: [{
//                 dataPoints: dps2
//             }],
//             slider: {
//                 minimum: new Date(2019, 08, 20),
//                 maximum: new Date(2019, 12, 31)
//             }
//         }
//     });
//     $.getJSON(`/frontend_stockdata_json/${stockname}_data.json`, function (data) {
//         for (var i = 0; i < data.length; i++) {
//             dps1.push({ x: new Date(data[i].Date), y: [Number(data[i].Open), Number(data[i].High), Number(data[i].Low), Number(data[i].Close)] });
//             dps2.push({ x: new Date(data[i].Date), y: Number(data[i].Close) });
//         }
//         stockChart.render();
//     });
// }
