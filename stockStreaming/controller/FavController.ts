import { FavService } from "../service/FavService";
import express from "express";
import HttpStatus from "http-status-codes";


export class FavController {
    constructor(private favService: FavService) { };

    // attach(routes: express.Router) {
    //     routes.post("/fav/add/:stockNum", this.add);
    //     routes.delete("/fav/remove/:stockNum", this.remove);
    //     routes.get("/fav/list/", this.list);
    //     routes.get("/fav/day/:stockNum", this.day);
    //     routes.get("/fav/year/:stockNum", this.year);
    //     routes.get("/fav/check/:stockNum", this.check);
    // };

    add = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const currentUser = req.session.user;
                const userID = currentUser.id;
               // console.log(currentUser)
                const stockNum = req.params.stockNum;
                const result = await this.favService.add(userID, stockNum);
                if (result[0]>0) {
                    return res.json({ success: true });
                };
                return res.status(HttpStatus.BAD_REQUEST).json({ success: false, message: "Already Bookmarked" });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Login Please." });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to add fav-stocks" });
        }
    };

    check = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                console.log('aaa')
                if (!req.session.user) {
                    return res.json({ success: false, message: "Please Login." });
                };
                const currentUser = req.session.user;
                const userID = currentUser.id;
                const stockNum = req.params.stockNum;
                const result = await this.favService.check(userID, stockNum);
                //console.log(stockNum)
                if (!result) {
                    return res.json({ success: true });
                };
                return res.json({ success: false, message: "Already Bookmarked" });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Login Please." });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to check" });
        }
    };

    remove = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const currentUser = req.session.user;
                const userID = currentUser.id;
                const stockNum = req.params.stockNum;
                const result = await this.favService.remove(userID, stockNum);
                if (result == 1) {
                    return res.json({ success: true });
                };
                return res.json({ success: false, message: "Already Delete" });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Failed to remove fav-stock" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to remove fav-stocks" });
        }
    };

    list = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                if (req.session.user) {
                    const currentUser = req.session.user;
                    const userID = currentUser.id;
                    const stocks = await this.favService.favList(userID)
                    if (!stocks[0]) {
                        return res.status(HttpStatus.SERVICE_UNAVAILABLE).json({ success: false })
                    };
                    return res.json(stocks);
                }
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to list fav-stocks" });
        }
    };

    day = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const stockNum = req.params.stockNum;
                const close = await this.favService.dayClose(stockNum)
                if (close) {
                    return res.json(close)
                };
                return res.status(HttpStatus.SERVICE_UNAVAILABLE).json({ success: false, message: "Invalid date" });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Login Please." });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed " });
        }
    };

    year = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const stockNum = req.params.stockNum;
                const yearDetial = await this.favService.yearData(stockNum)
                if (yearDetial) {
                    return res.json(yearDetial)
                };
                return res.status(HttpStatus.SERVICE_UNAVAILABLE).json({ success: false, message: "Invalid Year" });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Login Please." });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed " });
        }
    };
};

