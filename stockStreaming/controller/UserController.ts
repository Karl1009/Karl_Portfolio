import { UserService } from "../service/UserService";
import express from "express";
import HttpStatus from "http-status-codes";
import fetch from 'node-fetch';
import { checkPassword, hashPassword } from '../hash';

export class UserController {
    constructor(private loginService: UserService) { };

    attach(routes: express.Router) {
        routes.post("/login", this.login);
        routes.get("/google", this.loginGoogle);
        routes.get("/logout", this.logout);
        routes.post("/register", this.register);
        routes.get("/loginCheck", this.loginCheck);
    };

    register = async (req: express.Request, res: express.Response) => {
        let { username, password } = req.body;
        if (!username) {
            res
                .status(HttpStatus.BAD_REQUEST)
                .json({ success: false, message: "Missing E-mail" });
            return;
        }
        if (!password) {
            res
                .status(HttpStatus.BAD_REQUEST)
                .json({ success: false, message: "Missing Password" });
            return;
        }
        try {
            const result = await this.loginService.login(username);
            const hash = await hashPassword(password)
            const email = result[0]
            //console.log(email)
            if (!email) {
                const newUser = await this.loginService.register(username, hash);
                //console.log(username)
                //console.log(newUser)
                if (req.session) {
                    req.session.user = {
                        id: newUser
                    };
                }
                console.log(`id:${newUser}`)
                return res.json({ success: true })
            };
            return res.json({ success: false, message: "User already exist" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Register Failed" });
        }
    };

    loginGoogle = async (req: express.Request, res: express.Response) => {
        try {//console.log('hello world')
            const accessToken = req.session?.grant.response.access_token;
            const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
                method: "GET",
                headers: {
                    "Authorization": `Bearer ${accessToken}`
                }
            });
            const result = await fetchRes.json();
            console.log(result);
            const users = await this.loginService.login(result.email);
                        const user = users[0];           
            console.log(users)
            console.log(user)
            const token = await hashPassword(result.email)
            if (user) {
                if (req.session) {
                    req.session.user = {
                        id: user.id
                    };
                }
                //console.log(user)
                return res.redirect('/')
            } else if (user == undefined) {
                const newUser = await this.loginService.register(result.email, token);
                if (req.session) {
                    req.session.user = {
                        id: newUser
                    };
                }
                return res.redirect('/')
            };
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Login Failed" });
        }
    }

    logout = async (req: express.Request, res: express.Response) => {
        if (req.session) {
            delete req.session.user;
            //console.log(req.session.user)
        }
        res.json({ success: true })
    }

    login = async (req: express.Request, res: express.Response) => {
        let { username, password } = req.body;
        if (!username) {
            res
                .status(HttpStatus.BAD_REQUEST)
                .json({ success: false, message: "Missing E-mail" });
            return;
        }
        if (!password) {
            res
                .status(HttpStatus.BAD_REQUEST)
                .json({ success: false, message: "Missing Password" });
            return;
        }
        try {
            let user = await this.loginService.login(username);
            const result = user[0]

            if (!result) {
                return res.status(HttpStatus.UNAUTHORIZED).json(result);;
            }

            if (result.username == 'admin01@admin.com' && password == result.password) {
                if (req.session) {
                    req.session.user = {
                        id: result.id
                    };
                };
                console.log(`login id:${result.id}`)
                return res.json({ success: true });
            }

            if (result.username == 'user01@user.com' && password == result.password) {
                if (req.session) {
                    req.session.user = {
                        id: result.id
                    };
                };
                console.log(`id:${result.id}`)
                return res.json({ success: true });
            }
            let match = await checkPassword(password, result.password);
            if (match) {
                if (req.session) {
                    req.session.user = {
                        id: result.id
                    };
                };
                console.log(`id:${result.id}`)
                return res.json({ success: true });
            } else {
                return res.status(HttpStatus.UNAUTHORIZED).json(result);
            }

        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Login Failed" });
        }
    };

    loginCheck = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        if (req.session && req.session.user) {
            res.setHeader(
                "Cache-Control",
                "no-cache, no-store, must-revalidate"
            ); // HTTP 1.1.
            res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
            res.setHeader("Expires", "0"); // Proxies.
            res.json({ success: true, message: req.session.user });
            next();
        } else {
            res.status(401).json({ success: false })
        }
    };
}

