import { StockService } from "../service/StockService";
import express from "express";
import HttpStatus from "http-status-codes";


export class StockController {
    constructor(private stockService: StockService) { };

    attach(routes: express.Router) {
        routes.get("/stock/year/:stockNum", this.stockYear);
        routes.get('/stock/stocksList', this.stocksList);
        routes.get('/stock/:stockNum', this.stock);
    };

    stockYear = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const stockNum = req.params.stockNum;
                const result = await this.stockService.year_data(stockNum);
                               if (result) {
                    return res.json(result);
                };
            }
            return res.status(HttpStatus.SERVICE_UNAVAILABLE).json({ success: false, message: "Invalid Stock Num" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to find year data" });
        }
    };

    stocksList = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const result = await this.stockService.stocksList();
                if (result) {
                    return res.json(result);
                };
                return res.status(HttpStatus.SERVICE_UNAVAILABLE).json({ success: false, message: "Invalid Stock" });
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Need session" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to find stocks" });
        }
    };

    stock = async (req: express.Request, res: express.Response) => {
        try {
            if (req.session) {
                const stockNum = req.params.stockNum;
                const result = await this.stockService.stock(stockNum);
                if (result) {
                    return res.json(result);
                };
            }
            return res.status(HttpStatus.SERVICE_UNAVAILABLE).json({ success: false, message: "Invalid Stock Num" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to find year data" });
        }

    };

}