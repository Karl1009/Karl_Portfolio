import { ChartService } from "../service/ChartService";
import express from "express";
import HttpStatus from "http-status-codes";


export class ChartController {
    constructor(private chartService: ChartService) { };

    attach(routes: express.Router) {
        routes.get("/chart/:stockID", this.take);
    };

    take = async (req: express.Request, res: express.Response) => {
        try {
                const stockID = req.params.stockID; 
                if (stockID == null) {
                    res.status(400).json({ msg: "Please enter stockID" })
                    return;
                }
                const result = await this.chartService.take(stockID);
                if (result) {
                    return res.json(result);
                };
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Failed to select stocks" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "Failed to select stocks" });
        }
    };
  }
