import express from 'express';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import expressSession from 'express-session';
import Knex from 'knex'
import grant from 'grant-express'
import configs from './knexfile';
import path from 'path';
// import { chartRoutes } from './chartRoutes';

let knex = Knex(configs.development);
dotenv.config();
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const sessionMiddleware = expressSession({
    secret: 'Great artist steal',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
});

app.use(sessionMiddleware);


app.use(grant({
    "defaults": {
        "protocol": "http",
        "host": process.env.HOST || "localhost:8080",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile", "email"],
        "callback": "/google"
    },
}));

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    if (req.session) {
        if (!req.session.counter) {
            req.session.counter = 1;
        } else {
            req.session.counter++;
        }
        console.log(`\n Number of session requests: ${req.session.counter}`);
    }
    const requestDate = new Date();
    const requestTime = `[${requestDate.getFullYear()}-${requestDate.getMonth()}-${requestDate.getDate()} ${requestDate.getHours()}:${requestDate.getMinutes()}:${requestDate.getSeconds()}]`;
    console.log(`${requestTime} Request: ${req.path}`);
    next();
});

import { UserService } from './service/UserService';
import { UserController } from './controller/UserController';
import { FavService } from './service/FavService';
import { FavController } from './controller/FavController';
import { ChartService } from './service/ChartService';
import { ChartController } from './controller/chartController';
import { StockService } from './service/StockService';
import { StockController } from './controller/StockController';


const favService = new FavService(knex);
export const favController = new FavController(favService)

const userService = new UserService(knex);
export const userController = new UserController(userService)

const chartService = new ChartService(knex)
export const chartController = new ChartController(chartService)

const stockService = new StockService(knex)
export const stockController = new StockController(stockService)

import { routes } from './routes'

// app.use('/', chartRoutes);


app.use('/', routes);
app.use(express.static(path.join(__dirname, 'public')));



// app.use((req: Request, res: Response) => {
//     if (req.session) {
//         res.sendFile(path.join(__dirname, "public/404.html"));
//     }
// });

const PORT = 8080;
app.listen(PORT, () => {
    console.log(`listening to port: ${PORT}`);
    if (process.env.CI) {
        process.exit(0);
    }
});
