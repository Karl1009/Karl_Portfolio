import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

# Read data from file 'filename.csv' 
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later) 
data = pd.read_csv("./stock_Year_Data_For_Ai/ai_data.csv") 
# Preview the first 5 lines of the loaded data 
print(data.head())
