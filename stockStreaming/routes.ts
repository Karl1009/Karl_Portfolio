import express from 'express';
import { favController } from './main';
import { userController } from './main';
import { chartController } from './main';
import { stockController } from './main';

export const routes = express.Router();

//user routes
routes.post("/login", userController.login);
routes.get("/google", userController.loginGoogle);
routes.get("/logout", userController.logout);
routes.post("/register", userController.register);
routes.get("/loginCheck", userController.loginCheck);

// fav stocks
routes.post("/fav/add/:stockNum", favController.add);
routes.delete("/fav/remove/:stockNum", favController.remove);
routes.get("/fav/list/", favController.list);
routes.get("/fav/day/:stockNum", favController.day);
routes.get("/fav/year/:stockNum", favController.year);
routes.get("/fav/check/:stockNum", favController.check);

//chart routes
routes.get("/chart/:stockID", chartController.take);

// stock year data routes
routes.get("/stock/year/:stockNum", stockController.stockYear);
routes.get('/stock/stocksList', stockController.stocksList)
routes.get('/stock/:stockNum', stockController.stock)


//routes.get("/filter", filterController.filter)
