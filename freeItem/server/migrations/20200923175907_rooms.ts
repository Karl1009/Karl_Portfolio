import * as Knex from "knex";
const roomsTable = 'rooms'

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(roomsTable, (table) => {
        table.increments();
        table.integer('host_id').unsigned().notNullable();
        table.foreign('host_id').references('users.id');
        table.integer('user_id').unsigned().notNullable();
        table.foreign('user_id').references('users.id');
        table.integer('item_id').unsigned().notNullable();
        table.foreign('item_id').references('items.id');
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(roomsTable);
}

