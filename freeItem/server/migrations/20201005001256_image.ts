import * as Knex from "knex";

const imgTable = 'image'
export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(imgTable, (table) => {
        table.increments();
        table.integer('item_id').unsigned().notNullable();
        table.foreign('item_id').references('items.id');
        table.string('path').notNullable();
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(imgTable);
}

