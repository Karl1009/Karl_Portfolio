import * as Knex from "knex";

const forumTable = "forum"
const userTable = 'users'
const commentTable = 'comment'


export async function up(knex: Knex): Promise<void> {

  await knex.schema.createTable(commentTable, (table)=>{
      table.increments();
      table.integer('user_id').unsigned().notNullable();
      table.foreign("user_id").references(`${userTable}.id`)
      table.integer('discuss_id').unsigned().notNullable();
      table.foreign("discuss_id").references(`${forumTable}.id`)
      table.string('comment').notNullable();
      table.timestamps(false, true);
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(commentTable);

}
