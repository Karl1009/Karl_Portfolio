export interface User {
  id: number;
  username: string;
  display_name: string;
  password: string;
  picture: string;
  created_at: Date;
  updated_at: Date;
  lat: string;
  lng: string;
}

export interface Categories {
  id: number;
  category: string;
  is_forum: boolean;
  created_at: Date;
  updated_at: Date;
}

export interface Items {
  id: number;
  creater_id: number;
  category_id: number;
  title: string;
  intro: string;
  pickup_times: string;
  is_available: boolean;
  lat: string | null;
  lng: string | null;
  created_at: Date;
  updated_at: Date;
  image: string;
}

export interface Rooms {
  id: number;
  host_id: number;
  user_id: number;
  item_id: number;
  created_at: Date;
  updated_at: Date;
}

export interface ChatUser {
  room_id: number;
  host_id: number;
  user_id: number;
  display_name: string;
  picture: string;
  message: string;
  sendTime: Date;
  unread: number;
}
export interface ChatHost {
  room_id: number;
  user_id: number;
  host_id: number;
  display_name: string;
  picture: string;
  message: string;
  sendTime: Date;
  unread: number;
}

export interface Msgs {
  id: number;
  room_id: number;
  sender_id: number;
  receiver_id: number;
  message: string;
  is_read: boolean;
  created_at: Date;
  updated_at: Date;
}



declare global {
  namespace Express {
    interface Request {
      user?: {
        id: number;
        username: string;
        display_name: string;
        picture: string;
        lat: string;
        lng: string;
        created_at: Date;
        updated_at: Date;

      };
    }
  }
}