import Knex from "knex";
import { Categories } from "./models";

export class CategoriesService {
    constructor(private knex: Knex) { }

    async getForumCats(): Promise<Categories[]> {
        const forumCats = await this.knex('categories').where('is_forum', 'true')
        return forumCats
    }

    async getItemCats(): Promise<Categories[]> {
        const itemCats = await this.knex('categories').where('is_forum', 'false')
        return itemCats
    }

}
