import { UserService } from '../services/UserService';
import { Request, Response } from 'express';
import HttpStatus from "http-status-codes";
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import fetch from 'node-fetch';
import { checkPassword, hashPassword } from '../hash';

export class UserController {
    constructor(private userService: UserService) { }

    register = async (req: Request, res: Response) => {
        try {
            console.log(req.body)
            console.log(req.body.data.display_name)
            const username = await this.userService.getUserByUsername(req.body.data.username);
            if (req.body.data.password !== req.body.data.re_password) {
                res.status(401).json({ msg: 'Re-enter password does not match the password' })
                return;
            }
            if (username) {
                res.status(401).json({ msg: 'email has been used, please try again' });
                return;
            }
            const display_name = await this.userService.getUserByDisplayName(req.body.data.display_name);
            if (display_name) {
                res.status(401).json({ msg: 'username has been used, please try again' });
                return;
            }
            console.log('HI!')
            const password = await hashPassword(req.body.data.password);
            const id = await this.userService.createAccount(req.body.data.display_name, req.body.data.username, password, req.body.data.picture);
            console.log(id)
            if (id) {
                res.json({ msg: `Create Account ${id} success is true` });
                return;
            }
        } catch (err) {
            console.log(err.msg);
            res.status(500).json({ msg: 'internal server error' });
        }
    };

    login = async (req: Request, res: Response) => {
        try {
            if (!req.body.username || !req.body.password) {
                res.status(401).json({ msg: 'Missing Username/Password' });
                return;
            }
            const { username, password } = req.body;
            const user = await this.userService.getUserByUsername(username);
            if (!user || !(await checkPassword(password, user.password))) {
                res.status(401).json({ msg: 'Wrong Username/Password' });
                return;
            }
            const payload = {
                id: user.id,
            };
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            res.json({ token });
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    };

    loginGoogle = async (req: Request, res: Response) => {
        try {
            console.log('loginGoogle')
            if (!req.body.response) {
                res.status(401).json({ msg: "no user information" });
                return;
            }
            const token_id = req.body.response.tokenId;
            const fetchResponse = await fetch(`https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=${token_id}`);
            const result = await fetchResponse.json();

            if (result.error) {
                res.status(401).json({ msg: 'Wrong access Token!' });
                return;
            }
            let payload: { id: number };
            console.log(result)
            const user = await this.userService.getUserBySocialID('google', result.sub);

            if (!user) {

                const password = await hashPassword('jasonishandsome');
                const userID = await this.userService.createUserWithSocial(
                    result.email,
                    result.name,
                    password,
                    'google',
                    result.sub,
                    result.picture,
                );
                payload = { id: userID };
            } else {
                // user found
                payload = { id: user.id };
                console.log(user.id)
            }
            console.log(payload)
            console.log("before require key")
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            console.log('after require key')
            res.json({
                token: token,
            });

        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    loginFacebook = async (req: Request, res: Response) => {
        try {
            if (!req.body.accessToken) {
                res.status(401).json({ msg: "Wrong Access Token!" });
                return;
            }

            const { accessToken } = req.body;
            const fetchResponse = await fetch(`https://graph.facebook.com/me?access_token=${accessToken}&fields=id,name,email,picture`);
            const result = await fetchResponse.json();
            console.log(result)
            if (result.error) {
                res.status(401).json({ msg: 'Wrong access Token!' });
                return;
            }

            let payload: { id: number };
            const user = await this.userService.getUserBySocialID('facebook', result.id);
            if (!user) {
                const password = await hashPassword('jasonishandsome');
                const userID = await this.userService.createUserWithSocial(
                    result.email,
                    result.name,
                    password,
                    'facebook',
                    result.id,
                    result.picture.data.url,
                );
                payload = { id: userID };
            } else {
                // user found
                payload = { id: user.id };
            }
            console.log(payload)
            console.log("before require key")
            const token = jwtSimple.encode(payload, jwt.jwtSecret);
            console.log("after require key")
            res.json({
                token: token,
            });
            return

        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }


    getUserInfo = async (req: Request, res: Response) => {
        try {
            console.log(req?.user)
            res.json({ user_info: req?.user });
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });

        }
    };

    getprofile = async (req: Request, res: Response) => {
        try {
            console.log(req.params)
            console.log(req.params.user_id)
            const user = await this.userService.getProfileInfo(req.params.user_id)
            res.json({ user });
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

    addForumLike = async (req: Request, res: Response) => {
        try {
            console.log(req.params.user_id)
            console.log(req.params.discuss_id)
            const discuss_id = await this.userService.addForumLike(req.params.user_id, req.params.discuss_id)
            res.json({ discuss_id });
            console.log(discuss_id)
            return
        } catch (err) {
            console.log(err.message);
            res.status(500).json({ message: 'internal server error' });
        }
    }

   setLocation = async (req: Request, res: Response) => {
        try {
            if (req.user) {             
                const userId = req.user.id;
                const lat = req.body.lat;
                const lng= req.body.lng;
                const location = await this.userService.setLocation(userId,lat,lng)           
                return res.json({ success: true, message:location[0]});
            }
            return res.status(HttpStatus.UNAUTHORIZED).json({ success: false, message: "Please Login!" });
        } catch (e) {
            console.log(e);
            return res.status(500).json({ message: "read msg : internal server error" });
        }
    };

}
