import { ForumService } from '../services/ForumService'
import { Request, Response } from 'express';
// 
export class ForumController {
  constructor(private forumService: ForumService) { };

  addSingleDiscussion = async (req: Request, res: Response) => {
    try {
      console.log(req.body)
      const user_id = req.body.user_id
      const title = req.body.title
      const category_id = req.body.category_id
      const description = req.body.description
      const hashTag = JSON.stringify((req.body.hashtag).split(","))
      const image = req.file ? req.file.filename : "";
      // const hashTag = temphashTag
      console.log(image)
      // console.log(hashTag)
    //   const {
    //     user_id,
    //     title,
    //     description,
    //     category_id,
    //     image,
    // } = req.body;
    // const hashTag = JSON.stringify(req.body.hashtag)

      const data = await this.forumService.addSingleDiscussion(user_id, title, category_id, description, hashTag, image);

      res.json({ data });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };
  getAllDiscussion = async (req: Request, res: Response) => {
    try {

      const data = await this.forumService.getAllDiscussion(req.params.user_id);
      res.json({ data });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  getSingleDiscussion = async (req: Request, res: Response) => {
    try {
      let discuss_id = req.params.discuss_id

      const data = await this.forumService.getSingleDiscussion(discuss_id);
      res.json({ data });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  addSingleComment = async (req: Request, res: Response) => {
    try {

      const user_id = parseInt(req.body.data.user_id)
      const discuss_id = parseInt(req.body.data.discuss_id)
      const comment = req.body.data.comment
      console.log(discuss_id)
      const data = await this.forumService.addSingleComment(user_id, discuss_id, comment);
      res.json({ data });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  getAllComment = async (req: Request, res: Response) => {
    try {
      console.log('this is getallcomment')
      const data = await this.forumService.getAllComment(req.params.discuss_id);
      res.json({ data });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
    }
  };

  getAllForumCommentLength = async (req: Request, res: Response) => {
    try {
      console.log('this is getAllForumCommentLength')
      const data = await this.forumService.getAllForumCommentLength(req.params.discuss_id);
      res.json({ data });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
  }
};
getAllDiscussionByUserID = async (req: Request, res: Response) => {
  try {
      
     const data = await this.forumService.getAllDiscussionByUserID(req.params.user_id);
     res.json({data});
  } catch (err) {
      console.log(err.message);
      res.status(500).json({ message: 'internal server error' });
  }
};

removeForumDiscussion = async (req: Request, res: Response) => {
  try {
    console.log(req.params.discuss_id)
    await this.forumService.removeForumDiscussion(req.params.discuss_id);
    
    res.json({message:'success is true'}) 
  } catch (err) {
    console.log(err.message);
    res.status(500).json({message: 'internal server error'});
  }
}

}


//    createDiscussion = async (req: Request, res: Response) => {
//       try {

//          console.log('helloworld')
//          const user_id = req.body.user_id
//          const title = req.body.title
//          const categories = req.body.categories
//          const description = req.body.description
//          const hashTag = req.body.hashTag

//          const user_id = 1;
//          const title= '123';
//          const categories = '123';
//          const description = '123';
//          const hashTag = '123';
//          const discussID = await this.forumService.createDiscussion(user_id, title, categories, description, hashTag);
//          res.json({discussID});
//          return;
//       } catch (e) {
//          console.log(e);
//          return res.status(500).json({ message: "internal server error" });
//       }
//    };
// }