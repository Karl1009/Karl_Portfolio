import express from "express";

import { isLoggedIn, itemsController } from "../main";
import { upload } from "../main";

export const itemsRoutes = express.Router();

itemsRoutes.get("/available", itemsController.getAllItems);
itemsRoutes.get("/info/:item_id", itemsController.getItemInfo);
itemsRoutes.get("/image/:item_id", itemsController.getImage);
itemsRoutes.get("/user/:user_id", itemsController.getAllCategoriesItemByUserID);
itemsRoutes.delete("/removeitem/:item_id", itemsController.removeSingleItem);

itemsRoutes.post("/submit", upload.fields([
    { name: 'image', maxCount: 5 },]), isLoggedIn, itemsController.createItem);

itemsRoutes.put("/pickup/:id", isLoggedIn,itemsController.arrangePickup);


