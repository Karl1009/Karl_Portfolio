import express from "express";
import { categoriesController } from "../main";

export const categoriesRoutes = express.Router();

categoriesRoutes.get("/forum", categoriesController.getForumCats);
categoriesRoutes.get("/item", categoriesController.getItemCats);



