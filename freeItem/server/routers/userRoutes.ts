import express from 'express';
import { userController } from '../main';
import { isLoggedIn } from '../main';

export const userRoutes = express.Router();

userRoutes.post('/login', userController.login);
userRoutes.post('/login/facebook', userController.loginFacebook);
userRoutes.get('/info', isLoggedIn, userController.getUserInfo);
userRoutes.post("/login/google", userController.loginGoogle);
userRoutes.post('/register', userController.register);
userRoutes.get('/profile/:user_id',userController.getprofile)
userRoutes.get('/:user_id/forumliked/:discuss_id', userController.addForumLike)
userRoutes.put('/location',isLoggedIn, userController.setLocation);