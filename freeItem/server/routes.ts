import express from "express";
import { chatroomRoutes } from "./routers/chatroomRoutes";
import { userRoutes } from "./routers/userRoutes";
import { forumRoutes } from "./routers/forumRoutes";
import { categoriesRoutes } from "./routers/categoriesRoutes";
import { itemsRoutes } from "./routers/itemsRoutes";

export const routes = express.Router();

routes.use("/user", userRoutes);
routes.use("/forum", forumRoutes);
routes.use("/inbox", chatroomRoutes);
routes.use("/items", itemsRoutes);
routes.use("/categories", categoriesRoutes);
