# Take-two

TakeTwo wants to connect people who are willing to share their useful second-hand items to their 
society. We want to build communities for those who keen on saving 
valuable items and giving advices for recycling tips.


# Functions:

1. Our website supports facebook and google login for users to login with ease. Users can browse the listing page for second-hand items with or without login. By clicking on the items, users can easily check the items’ location and available time in detail. Users can set both their location through google map. 

2. During item submission, users can upload photos, input details, and set locations thru google map. Users can easily recognize the distance between his/her location and the items.

3. Our platform also supports chating functions. Users can easily have conversations with the item’s owner for further location and time detail for taking the item. Our chatroom is created in terms of each item, host, and user. It is built with socket io with an immediate response. 

4. We create a forum page for users to create posts whatever they want to share about environmental issues. 
Users can interact with the poster by leaving comments below the posts.