import { Button} from '@material-ui/core';
import logo from '../../Google__G__Logo.svg'
import React from 'react'
import GoogleLogin from 'react-google-login'
import {useDispatch} from 'react-redux';
import {loginGoogle} from '../../redux/auth/thunks'
import styles from '../../css/socialMediaButton.module.css'
const GoogleLoginApp=()=> {
  const dispatch = useDispatch();
  const responseGoogle = (response :any) => {
    //console.log(response)
    if (response) {
      dispatch(loginGoogle(response));
  }
}
  const responseGoogleFail = (response: any) => {
  }

  
      
  return (
    <>

    <GoogleLogin  
    clientId="1050110744801-um3bnjoi8jv9s4ctg18fbcdr3rbg2epu.apps.googleusercontent.com"
    render={renderProps => (
      <Button  className={styles.googlebutton} onClick={renderProps.onClick} disabled={renderProps.disabled} ><img src={logo}  className={styles.size} alt="GoogleLogo"/>SIGN IN WITH GOOGLE</Button>
    )}
    buttonText="Login"
    onSuccess={responseGoogle}
    onFailure={responseGoogleFail}
    cookiePolicy={'single_host_origin'}
  
  />
  </>
  )
}

export default GoogleLoginApp
