import React, { useEffect, useState } from "react";
import Paper from "@material-ui/core/Paper";
import "../css/SubmitForm.css";
import { MenuItem, TextField } from "@material-ui/core";
import Button from "@material-ui/core/Button/Button";
import { useDispatch, useSelector } from "react-redux";
import Input from "reactstrap/lib/Input";
import FormGroup from "reactstrap/lib/FormGroup";
import { thunkAllMsgs } from "../redux/Inbox/thunk";
import { useForm } from 'react-hook-form';
import { IRootState } from "../redux/store";
import { thunkItemCats } from "../redux/categories/thunks";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
import { Circle } from "@react-google-maps/api";
import MyLocationSharpIcon from "@material-ui/icons/MyLocationSharp";
import { Alert } from "reactstrap";

// const useStyles = makeStyles((theme: Theme) =>
//     createStyles({
//         container: {
//             display: "flex",
//             flexWrap: "wrap",
//         },
//         textField: {
//             marginLeft: theme.spacing(1),
//             marginRight: theme.spacing(1),
//             width: 200,
//         },
//     })
// );

const SubmitForm: React.FC = () => {
    const dispatch = useDispatch();
    const catsId = useSelector((state: IRootState) => state.categories.catsByItemForum['item']);
    const cats = useSelector((state: IRootState) => catsId?.map(id => state.categories.catsById[id]));
    const [category, setCategory] = useState(0);
    const [listing_days, setListingDays] = useState(0)
    const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
    const [mapRef, setMapRef] = useState(null);
    const lat = userInfo.lat ? parseFloat(userInfo.lat) : 22.3;
    const lng = userInfo.lng ? parseFloat(userInfo.lng) : 114;
    const [center, setCenter] = useState({ lat: lat, lng: lng });
    const [position, setPosition] = useState({ lat: lat, lng: lng });
    const [itemLocation, setItemLocation] = useState({ lat: lat, lng: lng });

    const handleCatsChange = (event: React.ChangeEvent<{ value: any }>) => {
        setCategory(event.target.value);

    };

    const handleDayChange = (event: React.ChangeEvent<{ value: any }>) => {

        setListingDays(event.target.value)
    };

    function handleCenterChanged() {
        if (!mapRef) return;
        //@ts-ignore
        setPosition(mapRef.getCenter().toJSON());
    }

    function locateMe() {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                //console.log("Latitude is :", position.coords.latitude);
                //console.log("Longitude is :", position.coords.longitude);
                setItemLocation({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                })
                setCenter({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                });
            },
            function (error) {
                console.error(
                    "Error Code = " + error.code + " - " + error.message
                );
            }
        );
    }

    useEffect(() => {
        locateMe();
    }, []);

    const options = {
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        clickable: false,
        draggable: false,
        editable: false,
        visible: true,
        zIndex: 1,
    };

    const { register, handleSubmit, reset, errors, clearErrors } = useForm();

    let days: number[] = [];
    let i;
    for (i = 0; i < 29; i++) {
        days.push(i)
    }

    const onSubmit = async (data: any) => {
        console.log(data)
        clearErrors()
        data["category_id"] = category;
        data["listing_days"] = listing_days;
        data['lat'] = itemLocation.lat;
        data['lng'] = itemLocation.lng;
        if (mapRef === null) { return alert("Please select your location") }
        if (data.category_id === 0) { return alert(" Please select category.") }
        if (data.listing_days === 0) { return alert(" Please select listing day.") }

        const formData = new FormData();
        formData.append('title', data.title);
        formData.append('intro', data.intro);
        formData.append('pickup_times', data.pickup_times);
        formData.append('category_id', data.category_id);
        formData.append('listing_days', data.listing_days);
        formData.append('lat', data.lat);
        formData.append('lng', data.lng);
        for (let i = 0; i < data.image.length; i++) {
            formData.append(`image`, data.image[i])
        }


        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/submit`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
            },
            body: formData,
        })
        const json = await res.json();
        //console.log(json)
        if (json[0]) {
            reset({
                title: undefined,
                intro: undefined,
                pickup_times: undefined,
                image: undefined,

            });
            setCategory(0);
            setListingDays(0);
            alert(" Post Success !")
        }

    }

    // const imgPreview = (e: React.ChangeEvent<HTMLInputElement>) => {
    //     if (e.target.files) {
    //         setObjectURL(URL.createObjectURL(e.target.files[0]));
    //     }
    // };


    useEffect(() => {
        dispatch(thunkAllMsgs());
    }, [dispatch]);

    useEffect(() => {
        dispatch(thunkItemCats());
    }, [dispatch]);

    return (
        <form onSubmit={handleSubmit(onSubmit)} className='submit-width' >
            <Paper component="form" className="SubmitForm">
                <TextField
                    className='title'
                    name="title"
                    label="Title"
                    color="secondary"
                    variant="outlined"
                    required
                    inputRef={register({ required: true, minLength: 1 })}
                />
                <TextField
                    className='intro'
                    name="intro"
                    label="Description"
                    color="secondary"
                    variant="outlined"
                    placeholder="e.g. 2 x cases Lotus Seed Paste Mooncake."
                    rows={4}
                    rowsMax={5}
                    required
                    inputRef={register({ required: true, minLength: 1 })}
                    multiline={true}
                />
                <TextField
                    className='pickup-time'
                    name="pickup_times"
                    label="Pick up times"
                    color="secondary"
                    variant="outlined"
                    placeholder="e.g. Monday to Friday 5-7 pm or Saturday afternoon."
                    required
                    inputRef={register({ required: true, minLength: 1 })}
                />
                <div className="options">
                    <TextField

                        select
                        name="category_id"
                        label="Category"
                        variant="outlined"
                        size="small"
                        value={category}
                        onChange={handleCatsChange}
                        required
                    > <MenuItem key={0} value={0}></MenuItem>
                        {cats &&
                            cats.map(cat => (
                                <MenuItem key={cat.id} value={cat.id}>{cat.category}</MenuItem>
                            ))}
                    </TextField>
                    <TextField

                        select
                        label="List for ... day(s)"
                        variant="outlined"
                        size="small"
                        name='listing_days'
                        value={listing_days}
                        onChange={handleDayChange}
                        required
                    >
                        <MenuItem key={0} value={0}></MenuItem>
                        {days.map(i => (<MenuItem key={i + 1} value={i + 1}>{i + 1}</MenuItem>))}
                    </TextField>
                </div>
                <FormGroup className='upload'>
                    <div>Up to 5 images:</div>
                    <Input
                        type="file"
                        name="image"
                        id="exampleFile"
                        accept="image/*"
                        multiple={true}
                        innerRef={register}
                    />
                </FormGroup>
                 <div className='key'>  <LoadScript googleMapsApiKey="AIzaSyBVyGg4eKKvNl4M2utwWorZ3jUdlFdiZZQ" >
                <div className='set-map'>       <Button variant="outlined" color="secondary"
                    onClick={() => (setItemLocation({ lat: position.lat, lng: position.lng }))}>
                    Set pick up location   (approx)</Button>
                    <Button color="secondary" onClick={() => locateMe()}>
                        <MyLocationSharpIcon /> Locate Me
                    </Button>
                </div>
               
                    <GoogleMap
                        onLoad={(map: any) => setMapRef(map)}
                        onCenterChanged={handleCenterChanged}
                        zoom={15}
                        center={center}
                        mapContainerStyle={{
                            height: "400px",
                            width: "100%",
                        }}
                    >                    {/* {console.log(userInfo)} */}
                        <Marker position={position} />
                        <Circle
                            center={itemLocation}
                            radius={70}
                            options={options}
                        />
                    </GoogleMap>
                </LoadScript>                </div>
                <Button
                    className="submitButton"
                    variant="contained"
                    color="secondary"
                    type="submit"
                >
                    Submit
            </Button>
                {errors.title && <Alert color="danger">{errors.title && "Please enter title"}</Alert>}
                {errors.intro && <Alert color="danger">{errors.intro && "Please enter description"}</Alert>}
            </Paper >
        </form>
    );
};

export default SubmitForm;
