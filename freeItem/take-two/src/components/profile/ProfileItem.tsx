
import React, { useEffect, useState } from 'react'
import { List, Paper,Checkbox } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { IRootState } from '../../redux/store';
import CategoriesItem from '../CategoriesItem';
import styles from '../../scss/profileCategoriesList.module.scss'
import { thunkUserShare } from '../../redux/items/thunks';
import { AiFillDelete } from "react-icons/ai"
import { setArrange } from '../../redux/items/actions';
import { Alert } from 'reactstrap';

const ProfileItem: React.FC = () => {
  const dispatch = useDispatch();
  const userInfo = useSelector((state: IRootState) => state.auth.userInfo);
  const avaItemsId = useSelector((state: IRootState) => state.item.shareItemByPickUp['true']);
  const pickedItemsId = useSelector((state: IRootState) => state.item.shareItemByPickUp['false']);
  const avaItems = useSelector((state: IRootState) =>avaItemsId?.map(id => state.item.userShareByItemId[id]));
   const pickedItems = useSelector((state: IRootState) =>pickedItemsId?.map(id => state.item.userShareByItemId[id]));
 
  const [message, setMessage] = useState<null | string>(null);
  useEffect(() => {
    dispatch(thunkUserShare(userInfo.id));
  }, [dispatch, userInfo.id]);


  const { REACT_APP_API_SERVER } = process.env
  return (
    <> <div  className={styles.title}>Available items</div>
      <Paper className={styles.main_page}>        
      {/* {console.log(itemsId)} {console.log(items)} */}      
        {(avaItems?.length=== 0 && message === null)&& <h3>No available item shared</h3>}
        {message && <div className={styles.deleteAlertCenter}><Alert color="success" className={styles.deleteAlert} >{message}</Alert></div>}
        {avaItems && avaItems.length > 0 && (
          <List className={styles.CategoriesList}>
            {avaItems.map((item, idx) => (
              <div>
                <button onClick={async () => {
                  const res = await fetch(`${REACT_APP_API_SERVER}/items/removeitem/${item.id}`, {
                    method: 'DELETE'
                  })
                  if (res.status === 200) {
                    const json = await res.json()
                    const item_title = json.item_title[0]
                    setMessage("Your item " + item_title + " has been deleted")
                    dispatch(thunkUserShare(userInfo.id))
                    setTimeout( () => ( 
                      setMessage(null))
                      , 5000)
                  }
                }
                } className={styles.deleteButton}><AiFillDelete className={styles.delete} /></button>
                <CategoriesItem
                  // key={`categories_${idx}`}                         
                  id={item.id}
                  //   image={item.image}
                  display_name={item.display_name}
                  picture={item.picture}
                />
                <FormControlLabel control={<Checkbox
                  checked={!item.is_available} onChange={async (event) => {
                    const token = localStorage.getItem('token');
                    const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/pickup/${item.id}`, {
                      method: 'PUT',
                      headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json'
                      },
                      body: JSON.stringify({
                        is_available: event.currentTarget.checked ?  'false' :'true'
                      }),
                    })
                  const json = await res.json();
                //  console.log(json)
                    dispatch(setArrange(json))
                  }} name="checked" />} label="Pick up arranged?" />
              </div>
            ))}
          </List>
        )}
      </Paper>
      <div  className={styles.title}>Pickup arranged</div>
      <Paper className={styles.main_page}>
       
      {/* {console.log(itemsId)}
        {console.log(items)} */}
        {(pickedItems?.length=== 0 && message === null)&& <h3>No item picked</h3>}
        {/* {message && <div className={styles.deleteAlertCenter}><Alert color="success" className={styles.deleteAlert} >{message}</Alert></div>} */}
        {pickedItems &&pickedItems.length > 0 && (
          <List className={styles.CategoriesList}>
            {pickedItems.map((item, idx) => (
              <div>
                {/* <button onClick={async () => {
                  const res = await fetch(`${REACT_APP_API_SERVER}/items/removeitem/${item.id}`, {
                    method: 'DELETE'
                  })
                  if (res.status === 200) {
                    const json = await res.json()
                    const item_title = json.item_title[0]
                    setMessage("Your item " + item_title + " has been deleted")
                    dispatch(thunkUserShare(userInfo.id))
                    setTimeout( () => ( 
                      setMessage(null))
                      , 5000)
                  }
                }
                } className={styles.deleteButton}><AiFillDelete className={styles.delete} /></button> */}
                <CategoriesItem
                  // key={`categories_${idx}`}                         
                  id={item.id}
                  //   image={item.image}
                  display_name={item.display_name}
                  picture={item.picture}
                />
                <FormControlLabel control={<Checkbox
                  checked={!item.is_available} onChange={async (event) => {
                    const token = localStorage.getItem('token');
                    const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/pickup/${item.id}`, {
                      method: 'PUT',
                      headers: {
                        Authorization: `Bearer ${token}`,
                        'Content-Type': 'application/json'
                      },
                      body: JSON.stringify({
                        is_available: event.currentTarget.checked ?  'false' :'true'
                      }),
                    })
                  const json = await res.json();
                 // console.log(json)
                    dispatch(setArrange(json))
                  }} name="checked" />} label="Pick up arranged?" />
              </div>
            ))}
          </List>
        )}
      </Paper>
    </>
  );
};

export default ProfileItem;
