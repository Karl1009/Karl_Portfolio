import React, { useCallback, useEffect} from 'react'
import useReactRouter from 'use-react-router';
import { useDispatch } from 'react-redux';

import { Paper } from '@material-ui/core';

import '../scss/userprofile.scss'
import { thunkAllMsgs } from '../redux/Inbox/thunk';



// const { REACT_APP_API_SERVER } = process.env

const UsersProfile = () => {

  const dispatch = useDispatch();
  const router = useReactRouter<{ user_id: string }>();
  const user_id: number = parseInt(router.match.params.user_id);


  useCallback(async () => {
    if (user_id == null) {
      return;
    }
    // const res = await fetch(`${REACT_APP_API_SERVER}/user/profile/` + user_id)
    // // const json = await res.json();

  //  console.log(json)

  }, [user_id])

  useEffect(() => {
    dispatch(thunkAllMsgs())
  }, [dispatch]);

  return (
    <div className="main_page">
      <Paper className="Paper">
        {/* <h1 >This is comment room.</h1>
    <h2>{room.discuss_id}</h2>
    <h2 onClick={()=> {dispatch(push(`/forum/${room.user_id}`))}}>{room.username}</h2>
  <h2>{room.picture}</h2>
  <h2>{room.display_name}</h2>
  <h2>{room.created_at}</h2>
  <h2>{room.description}</h2>
  <h2>{room.categories}</h2>
  <h2>{room.hash_tag}</h2>
  <h2>{room.title}</h2>
  <h2>{room.user_id}</h2>

       */}
      </Paper>
    </div>
  )
}

export default UsersProfile
