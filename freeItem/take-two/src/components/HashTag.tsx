import React, { useState } from 'react'
import '../scss/test.scss'

export const HashTag =()=> {
const [state, setState] = useState({tags:["HashTag here"]})


  const removeTag = (i:any) => {
    
    const newTags = [ ...tags ];
    newTags.splice(i, 1);
    setState({ tags: newTags });
  }
  const { tags } = state;
  const inputKeyDown = (event:any) => {
    const val = event.target.value;
    if (event.key === 'Enter' && val) {
      event.preventDefault();
      if (tags.find(tag => tag.toLowerCase() === val.toLowerCase())) {
        return;
      }
      event.preventDefault();
      setState({ tags: [...tags, val]});
      event.target.value = null;
    } else if (event.key === 'Backspace' && !val) {
      removeTag(tags.length - 1);
    }
  }
    return (
      <div className="input-tag">
        <ul className="input-tag__tags">
          { tags.map((tag, i) => (
            <li key={tag}>
              {tag}
              <button type="button" onClick={() => { removeTag(i); }}>+</button>
            </li>
          ))}
          <li className="input-tag__tags__input"><input type="text" onKeyDown={inputKeyDown}/></li>
        </ul>
      </div>
    );
  }

 