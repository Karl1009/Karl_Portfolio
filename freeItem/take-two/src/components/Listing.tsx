import { List } from "@material-ui/core";
import React, { useEffect } from "react";
import CategoriesItem from "./CategoriesItem";
import "../css/CategoriesList.css";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import { thunkAllItem } from "../redux/items/thunks";

// const {REACT_APP_UPLOAD} = process.env

export default function Listing(props: {
    catId: number
}) {
    const dispatch = useDispatch();

    const itemsId = useSelector((state: IRootState) => state.item.itemByCatId[props.catId]);
    const items = useSelector((state: IRootState) => itemsId?.map(id => state.item.allItemById[id]));
    console.log(itemsId)
    console.log(items)
    useEffect(() => {
        dispatch(thunkAllItem());
    }, [dispatch]);

    return (
        <>
            {items && items.length > 0 && (
                <List className="CategoriesList">
                    {items.map((item, idx) => (
                        <CategoriesItem
                            id={item.id}
                            display_name={item.display_name}
                            picture={item.picture}
                        />
                    ))}
                </List>
            )}
        </>
    );
};


