import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import { ListItem } from '@material-ui/core';
// @ts-ignore 
import { ChatItem, } from 'react-chat-elements'
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { push } from 'connected-react-router';
import { useRouteMatch } from 'react-router-dom';
import { thunkAllMsgs, thunkReadMsg } from '../../redux/Inbox/thunk';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            maxWidth: 360,
            backgroundColor: theme.palette.background.paper,
        },
        nested: {
            paddingLeft: theme.spacing(4),
        },
    }),
);

export function MyShareList(props: {
    roomId: number;
}) {
    const dispatch = useDispatch();
    const match = useRouteMatch<{ id: string }>();
    const userId = parseInt(match.params.id);
    const room = useSelector((state: IRootState) => state.inbox.itemRoomById[props.roomId])
    const msgId = useSelector((state: IRootState) => state.inbox.msgByRoomId[props.roomId]);
    const roomMsg = useSelector((state: IRootState) => msgId?.map(id => state.inbox.msgById[id]));
    const classes = useStyles();

    let count = 0
    for (const msg of roomMsg) {
        if (msg.receiver_id === userId) {
            if (msg.is_read === false) { count++; }
        }
    }

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    return (<>
        <ListItem button className={classes.nested} onClick={() => {
            const func = () => dispatch(push(`/inbox/share/${room.host_id}/room/${room.room_id}`));
            func();
            dispatch(thunkReadMsg(room.room_id))
        }}>
            <ChatItem
                avatar={room.picture}
                title={room.display_name}
                subtitle={room.message}
                date={new Date(room.created_at)}
                unread={count} />
        </ListItem>
    </>)
}



