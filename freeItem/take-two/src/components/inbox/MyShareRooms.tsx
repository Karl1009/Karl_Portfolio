import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store'
import { thunkAllMsgs, thunkItemRooms } from '../../redux/Inbox/thunk';
import { MyShareList } from './MyShareList';
import { List } from '@material-ui/core';

export function MyShareRooms(props: {
    itemId: number;
}) {
    const dispatch = useDispatch();
    const itemRoomsId = useSelector((state: IRootState) => state.inbox.roomByItemId[props.itemId]);
    const itemRooms = useSelector((state: IRootState) => itemRoomsId?.map(id => state.inbox.itemRoomById[id]))

    useEffect(() => {
        dispatch(thunkItemRooms(props.itemId))
    }, [props.itemId, dispatch])

    useEffect(() => {
        dispatch(thunkAllMsgs())
    }, [dispatch]);

    return (<>
        {itemRooms && itemRooms?.map((room,i) => (
            <List key={i} component="div" disablePadding>
                <MyShareList roomId={room.room_id} />
            </List>
        ))}
    </>)
}








