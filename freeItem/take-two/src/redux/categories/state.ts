export interface ICats {
    id: number
    category: string;
    is_forum: boolean;
}

export interface ICategoriesState  {
    catsById: {
        [catsId: string]: ICats,
    },
    catsByItemForum: {
        [ItemForum: string]: number[]
    }
}

export const initialState: ICategoriesState  = {
    catsById: {},
    catsByItemForum: {}

}

// export interface ISubmitItem {
//     id: number;
//     category_id: number;
//     title: string;
//     intro: string;
//     is_available: boolean;
//     image?: File;
//     expire_day: string;
//     lat: number;
//     lng: number;
// }

// export interface ISubmitState {
//     categoriesItems: Array<ISubmitItem>;
//     isProcessing: boolean;
// }

// export interface ICategoriesItem {
//     id: number;
//     category_id: number;
//     title: string;
//     intro: string;
//     pickup_times: string;
//     listing_days: string;
//     is_available: boolean;
//     image: string;
//     expire_day: Date;
//     lat: number;
//     lng: number;
//     created_at: Date;
//     display_name: string,
//     username: string,
//     picture: string,
// }

// export interface ICategoriesState {
//     categoriesItems: Array<ICategoriesItem>;
//     isProcessing: boolean;
// }

// export const initCategoriesState = {
//     categoriesItems: [] as Array<ICategoriesItem>,
//     isProcessing: false,
// };
