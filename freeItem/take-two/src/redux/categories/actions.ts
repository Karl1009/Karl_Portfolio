import { ICats } from "./state"


export function forumCats(forumCats: ICats[], forum:string) {
    return {
        type: '@@categories/FORUM' as '@@categories/FORUM',
        forumCats,
        forum,
    }
}


export function itemCats(itemCats: ICats[], item:string) {
    return {
        type: '@@categories/ITEM' as '@@categories/ITEM',
        itemCats,
        item,
    }
}

export type ICategoriesActions = ReturnType<
    typeof forumCats |
    typeof itemCats>



// import { ISubmitItem, ICategoriesItem } from "./state";

// export const SET_CATEGORIES_ITEMS = "@@categories/SET_CATEGORIES_ITEMS";

// export const SET_IS_PROCESSING = "@@categories/SET_IS_PROCESSING";
// export const ADD_CATEGORIES_SUCCESS = "@@categories/ADD_CATEGORIES_SUCCESS";
// export const ADD_CATEGORIES_FAIl = "@@categories/ADD_CATEGORIES_FAIl";

// interface ISetCategoriesItems {
//     type: typeof SET_CATEGORIES_ITEMS;
//     categoriesItems: Array<ICategoriesItem>;
// }

// interface ISetIsPorcessing {
//     type: typeof SET_IS_PROCESSING;
// }

// interface IAddCategoriesSucess {
//     type: typeof ADD_CATEGORIES_SUCCESS;
//     item: ISubmitItem;
// }

// interface IAddCategoriesFail {
//     type: typeof ADD_CATEGORIES_FAIl;
// }

// export const setCategoriesItems = (
//     categoriesItems: Array<ICategoriesItem>
// ): ISetCategoriesItems => {
//     return {
//         type: SET_CATEGORIES_ITEMS,
//         categoriesItems,
//     };
// };

// export const setIsPorcessing = () => {
//     return {
//         type: ADD_CATEGORIES_FAIl,
//     };
// };

// export const addCategoriesSucess = (
//     item: ISubmitItem
// ): IAddCategoriesSucess => {
//     return {
//         type: ADD_CATEGORIES_SUCCESS,
//         item,
//     };
// };

// export const addCategoriesFail = () => {
//     return {
//         type: ADD_CATEGORIES_FAIl,
//     };
// };

// export type ICategoriesActions =
//     | ISetCategoriesItems
//     | ISetIsPorcessing
//     | IAddCategoriesSucess
//     | IAddCategoriesFail;
