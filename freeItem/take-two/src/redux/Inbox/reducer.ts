import produce from 'immer'
import { InboxActions } from './action';
import { InboxState, initialState } from './state';


export function inboxReducers(state: InboxState = initialState, action: InboxActions): InboxState {
  return produce(state, state => {
    switch (action.type) {

      case '@@inbox/LOAD_ITEMS':
        if (action.items != null) {
          for (const item of action.items) {
            state.itemsById[item.id] = item

          }
          state.itemsByCreaterId[action.userId] = action.items.map(item => item.id);
        }
        break;

      case '@@inbox/LOAD_ITEMROOMS': 
        if (action.rooms != null) {
          for (const room of action.rooms) {
            state.itemRoomById[room.room_id] = room
          }
          state.roomByItemId[action.itemId] = action.rooms.map(room => room.room_id);
        }
       break;

      case '@@inbox/LOAD_USERROOMS': 
        if (action.rooms != null) {
          for (const room of action.rooms) {
            state.userRoomById[room.id] = room
            state.userRoomByItemId[room.item_id] = room.id
          };
          state.roomByUserId[action.userId] = action.rooms.map(room => room.id);
        }
       break;

      case '@@inbox/LOAD_ALL_MSGS': 
        if (action.msgs) {
          state.allMsgId = []
          for (const msg of action.msgs) {
            state.msgById[msg.id] = msg
            state.msgByRoomId[msg.room_id] = []
          }
          for (const msg of action.msgs) {
            state.msgByRoomId[msg.room_id].push(msg.id)
            state.allMsgId.push(msg.id)
          }
        }
       break;

      case '@@inbox/SEND_MSG': 
        //console.log(action.msg)
        state.msgById[action.msg.id] = action.msg;
        if (!state.msgByRoomId[action.msg.room_id]) {
          state.msgByRoomId[action.msg.room_id] = []
        }
        state.msgByRoomId[action.msg.room_id].push(action.msg.id)
        state.allMsgId.push(action.msg.id)
       break;

      case '@@inbox/READ_MSG': 
        for (const msg of action.msgs) {
          state.msgById[msg.id] = msg;
        };
      break;

      case '@@inbox/CREATEROOM': 
        state.userRoomByItemId[action.itemId] = action.roomId
       break;

    }
  })
}   