import { ChatHost, ChatUser, Item, Msg, } from "./state"

export function loadItems(items: Item[], userId: number) {
    return {
        type: '@@inbox/LOAD_ITEMS' as '@@inbox/LOAD_ITEMS',
        items,
        userId,
    }
}

export function loadItemRooms(rooms: ChatUser[], itemId: number) {
    return {
        type: '@@inbox/LOAD_ITEMROOMS' as '@@inbox/LOAD_ITEMROOMS',
        rooms,
        itemId,
    }
}

export function loadUserRooms(rooms: ChatHost[], userId: number) {
    return {
        type: '@@inbox/LOAD_USERROOMS' as '@@inbox/LOAD_USERROOMS',
        rooms,
        userId,
    }
}

export function loadAllMsgs(msgs: Msg[]) {
    return {
        type: '@@inbox/LOAD_ALL_MSGS' as '@@inbox/LOAD_ALL_MSGS',
        msgs,
    }
}

export function sendMsg(msg: Msg) {
    return {
        type: '@@inbox/SEND_MSG' as '@@inbox/SEND_MSG',
        msg,
    }
}

export function readMsg(msgs: Msg[]) {
    return {
        type: '@@inbox/READ_MSG' as '@@inbox/READ_MSG',
        msgs,
    }
}

export function createRoom(itemId:number,roomId:number) {
    return {
        type: '@@inbox/CREATEROOM' as '@@inbox/CREATEROOM',
        itemId,
        roomId,
    }
}

export type InboxActions = ReturnType<
    typeof loadItems |
    typeof loadItemRooms |
    typeof loadUserRooms |
    typeof loadAllMsgs |
    typeof sendMsg |
    typeof createRoom |
    typeof readMsg>