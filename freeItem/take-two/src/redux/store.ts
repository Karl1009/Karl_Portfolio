import { IAuthState } from "./auth/state";
import { authReducers } from "./auth/reducers";
import { IAuthActions } from "./auth/actions";
import { IdiscussState } from "./forum/state";
import { discussReducers } from "./forum/reducers";
import { IdiscussActions } from "./forum/actions";
import { createBrowserHistory } from "history";
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction,
} from "connected-react-router";
import { combineReducers, compose, applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { ThunkDispatch as OldThunkDispatch } from "redux-thunk";
import { inboxReducers } from "./Inbox/reducer";
import { InboxActions } from "./Inbox/action";
import { InboxState } from "./Inbox/state";
import { ICategoriesState } from "./categories/state";
import { ICategoriesActions } from "./categories/actions";
import { categoriesReducers } from "./categories/reducers";
import { IItemActions } from "./items/actions";
import { itemsReducers } from "./items/reducers";
import { IItemState } from "./items/state";

export const history = createBrowserHistory();

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}
export type ThunkDispatch = OldThunkDispatch<IRootState, null, IRootAction>;

export interface IRootState {
    item: IItemState
    categories: ICategoriesState;
    auth: IAuthState;
    forum: IdiscussState;
    inbox: InboxState;
    router: RouterState;
}

export const rootReducers = combineReducers<IRootState>({
    item: itemsReducers,
    categories: categoriesReducers,
    auth: authReducers,
    forum: discussReducers,
    inbox: inboxReducers,
    router: connectRouter(history),
});

export type IRootAction =
    | IItemActions
    | IAuthActions
    | CallHistoryMethodAction
    | InboxActions
    | IdiscussActions
    | ICategoriesActions

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState, IRootAction, {}, {}>(
    rootReducers,
    composeEnhancers(
        applyMiddleware(thunk),
        //    applyMiddleware(logger),
        applyMiddleware(routerMiddleware(history))
    )
);
