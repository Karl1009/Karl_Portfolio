import {initAuthState , IAuthState} from './state';
import {IAuthActions} from './actions'
import produce from 'immer'
// import { act } from 'react-dom/test-utils';

export function authReducers(state:IAuthState =initAuthState, action: IAuthActions):IAuthState {
  return produce(state, state => {
  switch(action.type){
    case '@@auth/LOGIN_IS_PROCESSING': 
        state.isAuthenticated= false;
        state.isProcessing= true;
      break;
    case '@@auth/LOGIN_SUCCESS': 
        state.isAuthenticated= true;
        state.isProcessing= false;
        state.userInfo = action.userInfo;
      break;
    case '@@auth/LOGIN_FAIL': 
        state.isAuthenticated =false;
        state.isProcessing = false;
        state.errMessage = action.message;
      break;
    case '@@auth/LOGOUT_SUCCESS': 
        state.isAuthenticated = false;
      break;
    case '@@auth/ADD_FORUM_LIKED': 
      state.userInfo.likedForum =  action.likedForum;
    break;
    case '@@auth/SET_LOCATION':  
      state.userInfo.lat =  action.userInfo.lat;
      state.userInfo.lng= action.userInfo.lng;
    break;
   
  }
})
}   