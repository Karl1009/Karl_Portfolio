import {IdiscussInfo, Icomment} from './state'
export const SET_IS_PROCESSING = '@@forum/DISCUSS_PROCESSING';
export const ADD_DISUCSS_SUCCESS ='@@forum/SET_DISUCSS_SUCCESS';
export const ADD_DISUCSS_FAIL = "@@forum/ADD_DISUCSS_FAIL";
export const SET_CONTENT = "@@forum/SET_CONTENT";
export const ADD_SINGLE_COMMENT = "@@forum/SET_SINGLE_COMMENT"
export const SET_ALL_COMMENT = "@@forum/SET_ALL_COMMENT"


interface IDiscussProcessing {
  type: typeof SET_IS_PROCESSING;
}

interface IaddDiscussSuccess {
  type: typeof ADD_DISUCSS_SUCCESS;
  item: IdiscussInfo;
}

interface IaddDiscussFail {
  type: typeof ADD_DISUCSS_FAIL;
}

interface IsetContent {
  type: typeof SET_CONTENT;
  content: Array<IdiscussInfo>
}

interface IaddSingleComment {
  type: typeof ADD_SINGLE_COMMENT;
  comment: Array<Icomment>;
}


interface IsetAllComment {
  type: typeof SET_ALL_COMMENT;
  comment: Array<Icomment>;
}



export const discussProcessing = (): IDiscussProcessing => {
  return{
  type: SET_IS_PROCESSING,
  }
}

export const addDiscussSuccess = (item:IdiscussInfo): IaddDiscussSuccess => {
  return {
    type: ADD_DISUCSS_SUCCESS,
    item,
  }
}

export const addDiscussfail = (): IaddDiscussFail => {
  return {
    type: ADD_DISUCSS_FAIL,
  }
}

export const setAllDiscussion =(content:Array<IdiscussInfo>): IsetContent => {
  return {
    type: SET_CONTENT,
    content,
  }
}

export const addSingleComment =(comment:Array<Icomment>): IaddSingleComment => {
  return {
    type: ADD_SINGLE_COMMENT,
    comment,
  }
}

export const setAllComment =(comment:Array<Icomment>): IsetAllComment => {
  return {
    type: SET_ALL_COMMENT,
    comment,
  }
}

export type IdiscussActions = IDiscussProcessing | IaddDiscussSuccess | IaddDiscussFail | IsetContent |IaddSingleComment | IsetAllComment ;