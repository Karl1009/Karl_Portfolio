export interface Icomment {
  user_id: number | null;
  display_name: string;
  picture: string;
  comment: string;
  discuss_id: number;
}



export interface IdiscussInfo {
  discuss_id: number;
  user_id: number | null;
  username: string;
  display_name: string;
  picture: string;
  title: string;
  category: string;
  description: string;
  hash_tag: string[];
  created_at: string;
  data: Array<Icomment>;
  liked: boolean;
  image: string;
}


export interface IdiscussState{
  isProcessing: boolean;
  content: Array<IdiscussInfo>;  // 拎到個key
  comments: Array<Icomment>; 

}

export const initDiscussState:IdiscussState  = {
  isProcessing: false,
  content: [],
  comments: [],
}