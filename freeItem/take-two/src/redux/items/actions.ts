import { IAllItems, IUserShare, IItemInfo, IImage } from "./state";


export function allItems(items: IAllItems[]) {
    return {
        type: '@@items/ALL_ITEMS' as '@@items/ALL_ITEMS',
        items,
    }
}

export function itemInfo(itemInfo: IItemInfo) {
    return {
        type: '@@items/INFO_ITEM' as '@@items/INFO_ITEM',
        itemInfo,
    }
}

export function userShare(userShares: IUserShare[], userId: number) {
    return {
        type: '@@items/USER_SHARE' as '@@items/USER_SHARE',
        userShares,
        userId,
    }
}

export function getImages(imgs: IImage[], itemId: number) {
    return {
        type: '@@items/GET_IMAGE' as '@@items/GET_IMAGE',
        imgs,
        itemId,
    }
}

export function newItem(itemInfo: IItemInfo) {
    return {
        type: '@@items/NEW_ITEM' as '@@items/NEW_ITEM',
        itemInfo
    }
}


export function setArrange(itemInfo: IItemInfo) {
    return {
        type: '@@items/ARRANGE_PICKUP' as '@@items/ARRANGE_PICKUP',
        itemInfo
    }
}


export type IItemActions = ReturnType<
    typeof setArrange |
    typeof newItem |
    typeof itemInfo |
    typeof allItems |
    typeof getImages |
    typeof userShare>



