import { ThunkDispatch } from "../store";
import { allItems, getImages, itemInfo, userShare } from "./actions";

export function thunkAllItem() {
    return async (dispatch: ThunkDispatch) => {    
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/available`)
        const json = await res.json();
        dispatch(allItems(json))
    }
}

export function thunkItemInfo(itemId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/info/${itemId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();
        console.log(json)
        dispatch(itemInfo(json))
    }
}

export function thunkUserShare(userId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/user/${userId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();
        dispatch(userShare(json, userId))
    }
}


export function thunkImages(itemId: number) {
    return async (dispatch: ThunkDispatch) => {
        const token = localStorage.getItem('token');
        const res = await fetch(`${process.env.REACT_APP_API_SERVER}/items/image/${itemId}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        })
        const json = await res.json();
        if (json[0]) {
            dispatch(getImages(json, itemId))
        }

    }
}
