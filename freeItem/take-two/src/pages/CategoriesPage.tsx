import React, { useEffect, useState } from "react";
import "../css/CategoriesPage.css";
import FullscreenIcon from "@material-ui/icons/Fullscreen";
import PhotoCameraIcon from "@material-ui/icons/PhotoCamera";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import BeachAccessIcon from "@material-ui/icons/BeachAccess";
import ChildCareIcon from "@material-ui/icons/ChildCare";
import HomeIcon from "@material-ui/icons/Home";
import CategoryIcon from "@material-ui/icons/Category";
import Listing from "../components/Listing";
import { useDispatch } from "react-redux";
import { thunkAllItem } from "../redux/items/thunks";


const CategoriesPage: React.FC = () => {
    const [catId, setCatId] = useState(7);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(thunkAllItem());
    }, [dispatch]);

    return (
        <div className="CategoriesPage">
            <div className="categoriesBar row">
                <div className="motherDiv " onClick={() => setCatId(7)}>
                    <div className="FullscreenIcon Icon">
                        <FullscreenIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">All</div>
                </div>
                <div className="motherDiv " onClick={() => setCatId(1)}>
                    <div className="PhotoCameraIcon Icon">
                        <PhotoCameraIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">Electronics</div>
                </div>
                <div className="motherDiv " onClick={() => setCatId(2)}>
                    <div className="LocalOfferIcon Icon">
                        <LocalOfferIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">Fashion</div>
                </div>
                <div className="motherDiv " onClick={() => setCatId(3)}>
                    <div className="BeachAccessIcon Icon">
                        <BeachAccessIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">Essentials</div>
                </div>
                <div className="motherDiv " onClick={() => setCatId(4)}>
                    <div className="ChildCareIcon Icon">
                        <ChildCareIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">Kids</div>
                </div>
                <div className="motherDiv " onClick={() => setCatId(5)}>
                    <div className="HomeIcon Icon">
                        <HomeIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">Furnitures</div>
                </div>
                <div className="motherDiv " onClick={() => setCatId(6)}>
                    <div className="CategoryIcon Icon">
                        <CategoryIcon style={{ color: "white" }} />
                    </div>
                    <div className="word">Others</div>
                </div>
            </div>
            <Listing catId={catId} />
        </div>
    );
};

export default CategoriesPage;
