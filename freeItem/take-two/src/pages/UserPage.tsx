import React from 'react'
// import logo from '../tidyup.jpg'
import styles from '../scss/form.module.scss'

const {REACT_APP_UPLOAD} = process.env

function UserPage() {

  return (
    <div>
      {/* <img className={styles.logo}src={logo} alt='logo'/> */}
      <img className={styles.logo} src={`${REACT_APP_UPLOAD}/58317.jpg`} alt=""/> 
    </div>
  )
}

export default UserPage
