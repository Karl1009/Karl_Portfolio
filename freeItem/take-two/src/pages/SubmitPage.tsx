import React from "react";
import SubmitForm from "../components/SubmitForm";
import "../css/SubmitPage.css";

const SubmitPage: React.FC = () => {
    return (
        <div className="SubmitPage">
            <SubmitForm />
        </div>
    );
};

export default SubmitPage;
