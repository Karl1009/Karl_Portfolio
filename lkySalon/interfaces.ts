export interface Clients {
    id?: number,
    clientName: string,
    phone: number,
    email: string,
    service_type: string,
    service_start: string,
    service_end?: string,
    stylist?: string
    // user_id: number
}

export interface Users {
    id: number,
    username: string,
    password: string
}

export interface service {
    service_type: string,
    service_time: number
}

// export interface Stylist {
//     user: string // link to stylistName
//     service: string // link to serviceType
// }

// // (hairWash, haircut, coloring, treatments, perms, others)
// export interface Services {
//     serviceType: string
//     price: number
// }
