create database project1;

\c project1;

create table Users(
    id SERIAL primary key,
    username VARCHAR(255) not null,
    password VARCHAR(255) not null,
    work_start TIME,
    work_end TIME,
    created_at TIMESTAMP default now(),
    update_at TIMESTAMP default now()
);

insert into Users (username, password, work_start, work_end) VALUES 
('Liz', '$2a$10$Mye.kV6vl8IYB.n49VvZ5uePJmIK6BI.6d4XbZxtXgX/ydwJVIKfi', '12:00:00+0800', '21:00:00+0800'),
('Karl', '$2a$10$Mye.kV6vl8IYB.n49VvZ5uePJmIK6BI.6d4XbZxtXgX/ydwJVIKfi', '12:00:00+0800', '21:00:00+0800'),
('Yin', '$2a$10$Mye.kV6vl8IYB.n49VvZ5uePJmIK6BI.6d4XbZxtXgX/ydwJVIKfi', '12:00:00+0800', '21:00:00+0800');



create table Clients(
    id SERIAL primary key,
    clientName VARCHAR(255) not null,
    phone INTEGER,
    email VARCHAR(255) not null,
    service_type text[],
    service_start TIMESTAMP,
    service_end TIMESTAMP,
    stylist VARCHAR(255) not null,
    user_id INTEGER,
    foreign key (user_id) references Users(id),
    created_at TIMESTAMP default now(),
    update_at TIMESTAMP default now()
);


insert into Clients (clientName, phone, email, service_type, service_start, service_end, stylist, user_id) VALUES 
('Alex', '91234567', 'alex@tecky.io','{"Hair Wash"}','2020-07-15 13:00:00', '2020-07-15 13:45:00','Liz',(SELECT id from Users where username ='Liz')),
('Gordon', '93456789', 'gordon@tecky.io','{"Hair Wash","Hair Cut"}','2020-07-16 14:00:00','2020-07-16 16:00:00','Karl',(SELECT id from Users where username ='Karl')),
('Andrew', '92345671', 'andrew@tecky.io','{"Hair Colouring"}','2020-07-15 14:00:00', '2020-07-15 17:00:00','Liz',(SELECT id from Users where username ='Liz')),
('Jason', '56789012', 'jason@tecky.io','{"Hair Cut"}','2020-07-16 12:30:00','2020-07-16 13:30:00','Karl',(SELECT id from Users where username ='Karl')),
('Dragon', '61234571', 'dragon@tecky.io','{"Hair Cut"}','2020-07-17 13:00:00', '2020-07-17 14:00:00','Yin',(SELECT id from Users where username ='Yin')),
('Chris', '56789012', 'chris@abc.com','{"Hair Wash","Hair Cut"}','2020-07-16 12:30:00','2020-07-16 14:30:00','Yin',(SELECT id from Users where username ='Yin'))

select * from clients where "stylist" = $1 AND (("service_start" < $2 AND $2 < "service_end") OR ("service_start" < $3 AND $3 < "service_end"))
select * from clients where "stylist" = 'Karl' AND (("service_start" < '2020-07-25 18:00:00' AND '2020-07-25 18:00:00' < "service_end") OR ("service_start"  < '2020-07-25 19:00:00' AND '2020-07-25 19:00:00' < "service_end"));

select * from clients where ("stylist" = 'Yin' AND ("service_start" between '2020-07-25 18:00:00 AND 2020-07-25 19:00:00') AND ("service_start" between '2020-07-25 18:00:00' and '2020-07-25 19:00:00'))