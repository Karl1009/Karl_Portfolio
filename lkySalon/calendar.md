        <!-- <div id="calendar"></div> -->

        <div class="card">
            <div class="card-body p-0">
              <div id="calendar"></div>
            </div>
        
        
        <!-- calendar modal -->
        <div id="modal-view-event" class="modal modal-top fade calendar-modal">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4 class="modal-title"><span class="event-icon"></span><span class="event-title"></span></h4>
                            <div class="event-body"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        
        <div id="modal-view-event-add" class="modal modal-top fade calendar-modal">
          <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
              <form id="add-event">
                <div class="modal-body">
                <h4>Schedule Appointment</h4>        
               <div class="form-group">
                    <label>Stylist</label>
                    <select class="form-control" name="eicon">
                      <option value="select">select stylist</option><span>(current)</span>
                      <option value="liz">Liz</option>
                      <option value="karl">Karl</option>
                      <option value="yin">Yin</option>
                    </select>
                  </div>  
                  <div class="form-group">
                    <label>Schedule Date</label>
                    <input type='text' class="datetimepicker form-control" name="edate">
                  </div>
         <div class="form-group">
                    <label>Services</label>
                    <select class="form-control" name="eicon">
                      <option value="select">select service</option><span>(current)</span>
                      <option value="hairWash">Hair Wash</option>
                      <option value="hairCut">Hair Cut</option>
                      <option value="colouring">Hair Colouring</option>
                      <option value="treatments">Treatments</option>
                      <option value="perms">Perms</option>
                      <option value="others">Others</option>
                    </select>
                  </div>                
                  <div class="form-group">
                    <label>Client Name</label>
                    <textarea class="form-control" name="edesc"></textarea>
                  </div>
                   <div class="form-group">
                    <label>Contact Number</label>
                    <textarea class="form-control" name="edesc"></textarea>
                  </div>
                   <div class="form-group">
                    <label>Email</label>
                    <textarea class="form-control" name="edesc"></textarea>
                  </div>
                 
              </div>
                <div class="modal-footer">
                <button type="submit" class="btn btn-primary" >Save</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>        
              </div>
              </form>
            </div>
          </div>
        </div>
    </div>
