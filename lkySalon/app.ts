import express,{Request,Response,NextFunction} from 'express';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
// import jsonfile from 'jsonfile';
import path from 'path';
import pg from 'pg';
import dotenv from 'dotenv';
import { isLoggedIn } from './guards';

dotenv.config();

const app = express();

export const client = new pg.Client({
    user:process.env.DB_USERNAME,
    password:process.env.DB_PASSWORD,
    database:process.env.DB_NAME,
    host:"localhost",
    // port:8080
})

// 呢個，係一開始連接，因為之後個connection 會一直都係通
client.connect();

// Middleware
app.use(expressSession({
    secret: 'Tecky',
    resave: true,
    saveUninitialized: true
}));

// 標準配備
app.use(bodyParser.urlencoded({extended:true})); // 普通form submit
app.use(bodyParser.json()); // json content
// 兩個都處理唔到file upload


app.use(function(req:Request, res:Response, next:NextFunction){
    if (req.session){
        if (!req.session.counter){
            req.session.counter = 1;
        } else {
            req.session.counter += 1;
        }
        console.log(`Counter: ${req.session.counter}`);
    }
    const now = new Date();
    console.log(`[${now.toISOString()}] Request ${req.path}`);
    next();
});


// app.post('/events', async function(req, res){
//     const events:Clients[] = await jsonfile.readFile(path.join(__dirname, 'events.json'))
//     events.push({
//         clientName: req.body.clientName,
//         phone: req.body.phone,
//         email: req.body.email,
//         service_type: req.body.service_type,
//         service_start: req.body.service_time,
//         service_end: req.body.service_time,
//         stylist: req.body.stylist,
//     });
//     await jsonfile.writeFile((path.join(__dirname, 'events.json')), events, {spaces:2});
//     res.json({success:true});
// });

import { userRoutes } from './userRoutes';
import { customerRoutes } from './customerRoutes';
// import { Clients } from './interfaces';
app.use('/user',userRoutes);
app.use('/',customerRoutes);

app.use(express.static('public'))

app.use('/admin',isLoggedIn,express.static('admin'));

app.use(function(req,res){
    res.sendFile(path.join(__dirname,'public/404.html'));
}); 

app.listen(8080,function(){
    console.log("Server listening on http://localhost:8080");
})

