let x = 0;
y1 = 100;
y2 = 135;
y3 = 170;
y4 = 205;
w = 40;
h = 115;

function setup() {
	const canvas = createCanvas(w, h);
	canvas.parent(document.querySelector('#canvas'));
}

function draw() {
	background(255);
	stroke(255);
	fill(13, 50, 117);
	quad(x, y1, x + 100, y1 - 50, x + 100, y1 - 30, x, y1 + 20);
	stroke(255);
	fill(13, 50, 117);
	quad(x, y2, x + 100, y2 - 50, x + 100, y2 - 30, x, y2 + 20);

	fill(13, 50, 117);
	quad(x, y3, x + 100, y3 - 50, x + 100, y3 - 30, x, y3 + 20);
	stroke(255);
	fill(13, 50, 117);
	quad(x, y4, x + 100, y4 - 50, x + 100, y4 - 30, x, y4 + 20);

	y1 = y1 - 2;
	y2 = y2 - 2;
	y3 = y3 - 2;
	y4 = y4 - 2;

	if (y1 < -13) {
		y1 = h + 13;
	}
	if (y2 < -13) {
		y2 = h + 13;
	}
	if (y3 < -13) {
		y3 = h + 13;
	}
	if (y4 < -13) {
		y4 = h + 13;
	}
}
