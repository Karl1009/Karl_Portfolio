var currentStylist = "empty";
var day;
var month;
var moment = document.createElement('script');  
moment.setAttribute('src','https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.27.0/moment.min.js');
document.head.appendChild(moment);

document.querySelector('#login-form')
  .addEventListener('submit', async function (event) {
    event.preventDefault();
    const form = event.target;
    const formObj = {};

    formObj['username'] = form.username.value;
    formObj['password'] = form.password.value;

    const res = await fetch('/user/login', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(formObj)

    });

    const result = await res.json();
    if (res.status === 200 && result.success) {
      checkLogin();
    } else {
      document.querySelector('#login-form-message').classList.remove('d-none');
      setTimeout(function () {
        document.querySelector('#login-form-message').classList.add('d-none');
      }, 5000);
    }
  });

async function checkLogin() {
  const res = await fetch('/current-user');
  // console.log(res)
  const user = await res.json();
  // console.log(user)
  if (user.login) {
    document.querySelector('#navbartext').innerHTML = `${user.username}，你已登入&nbsp;&nbsp;| `;
    document.querySelector('#logindropleft').classList.add('d-none');
    document.querySelector('#logout').classList.remove('d-none');
    document.querySelector('#navbartext').classList.remove('d-none');
    let a = document.querySelectorAll('.fa-edit')
    let b = document.querySelectorAll('.fa-trash')
    let c = document.querySelectorAll('.scheduleFontSize')
    let d = document.querySelectorAll('.booked')
    for (i = 0; i < a.length; i++) {
      a[i].classList.remove('d-none')
    }
    for (i = 0; i < b.length; i++) {
      b[i].classList.remove('d-none')
    }
    for (i = 0; i < c.length; i++) {
      c[i].classList.remove('d-none')
    }
    for (i = 0; i < d.length; i++) {
      d[i].classList.add('d-none')
      d[i].classList.add('moveToLeft')
    }
  }
}

// submission form -> service dropdown bar
let dropdown = document.getElementsByClassName('dropdown-btn');
let i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener('click', function () {
    this.classList.toggle('active');
    let dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === 'block') {
      dropdownContent.style.display = 'none';
    } else {
      dropdownContent.style.display = 'block';
    }
  });
};

document.querySelector('#date').innerHTML = getDate(new Date())
document.querySelector('.dropdown-menu')
  .addEventListener('click', async function (event) {
    // TODO
    // this is wrong because currentStylist at this moment is not defined, null
    currentStylist = event.target.innerHTML
    removeSchedule()
    document.querySelector('#stylist-name').innerHTML = `<div> ${currentStylist} </div>`;    // 要將currentStylist 寫係html 度
    // console.log(document.querySelector('#date').textContent)
    day = document.querySelector('#date').textContent.substring(0, 2);
    month = document.querySelector('#date').textContent.substring(3, 5);   // 要將日期拎入黎
    await addSchedule(day, month, currentStylist);

  });

const arrowDateSelect = document.querySelectorAll(".control-date > i")
let balanceOfClick = 0;
let offsetTimeZoneHours = new Date().getTimezoneOffset() / 60  //offsetTimeZone

for (let idx = 0; idx < arrowDateSelect.length; idx++) {
  arrowDateSelect[idx]
    .addEventListener('click', async () => {
      if (idx == 0) {
        balanceOfClick--;
      } else if (idx == 1) {
        balanceOfClick++;
      }
      thisDate = new Date(new Date().getTime() + 86400 * 1000 * balanceOfClick);
      day = String("0" + thisDate.getDate()).slice(-2)
      month = String("0" + (thisDate.getMonth() + 1)).slice(-2);
      document.querySelector("#date").innerHTML = getDate(thisDate)
      if (currentStylist == "empty") {
        console.log("no stylist selected")
      } else {
        removeSchedule();
        addSchedule(day, month, currentStylist);

      }
    });
}

async function addSchedule(day, month, currentStylist) {
  const res = await fetch("/name/" + currentStylist);
  const bookings = await res.json();


  for (booking of bookings) {
    if (String(booking.service_start).substring(8, 10) == String(day) &&  //day 核對日子
      String(booking.service_start).substring(5, 7) == String(month)) {    //month 核對月份
      let hours = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
      for (hour of hours) {
        if (String(("0" + (hour + offsetTimeZoneHours)).slice(-2)) ==
          String(booking.service_start).substring(11, 13)) {
          //核對時間 如有booking, 就進入去改schedule
          let duration = String(booking.service_end).substring(11, 13) - String(booking.service_start).substring(11, 13)
          // console.log(duration)
          memosContainerFirst = document.querySelector(`.h${hour}`);
          memosContainerFirst.innerHTML += `
        <div class="scheduleFontSize d-none"> 
          <p class="realScheduleFontSize">
          客戶名稱: ${booking.clientname} <br> 
          聯絡電話: ${booking.phone}  <br> 
          選擇服務: ${booking.service_type} <br> 
          </p>
        </div>
        <div class="booked">已預約
        </div>
        <div class="edit-button" data-id='${booking.id }'>
                        <i class="fas fa-edit  d-none" >
                        </i>
                    </div>
                    <div class="trash-button" data-id='${booking.id}'>
                        <i class="fas fa-trash d-none"></i>
                    </div>
        </div>
        `
          while (duration > 0) {
          memosContainerRepeat = document.querySelector(`.h${hour + duration -1}`);
          memosContainerRepeat.classList.add(`color${(String(booking.id).substr(String(booking.id).length - 1))}`)
          duration --;
        }
          checkLogin()
          setupTrashButtons()
        }
      }
    }
  }
}
function removeSchedule() {
  let hours = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21]
  for (hour of hours) {
    let memosContainer = document.querySelector(`.h${hour}`);
    memosContainer.innerHTML = ``;
    let colorNumber = 0;
    while (colorNumber < 10) {
      memosContainer.classList.remove(`color${colorNumber}`);  //Remove 10種顏色
      colorNumber++;
    }
  }
}
checkLogin()
function getDate(date) {
  let today = date;
  const weekday = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
  const dayOfWeek = weekday[today.getDay()];
  const day = String(today.getDate()).padStart(2, '0');
  const month = String(today.getMonth() + 1).padStart(2, '0');
  const year = today.getFullYear();
  today = `${day}/${month}/${year}, ${dayOfWeek}`;
  return today;
}

function setupTrashButtons() {
  const trashButtons = Array.from(document.querySelectorAll('.trash-button'));
  for (let trashButton of trashButtons) {
    trashButton.onclick = async function () {
      const id = trashButton.getAttribute('data-id');
      const res = await fetch(`/memos/${id}`, {
        method: "DELETE"
      });
      if (res.status === 401) {
        alert("Please login dude!");
        return;
      }
      const result = await res.json();
      console.log(result)
      console.log(day)
      console.log(month)
      console.log(currentStylist)
      removeSchedule()
      addSchedule(day, month, currentStylist)

      // 重新load 過最新版本
    };
  }
}

//others pop out value
const othersCheckbox = document.querySelector('input[id="others"]');
const othersText = document.querySelector('input[id="othersValue"]');
othersText.style.visibility = 'hidden';

othersCheckbox.addEventListener('change', () => {
  if (othersCheckbox.checked) {
    othersText.style.visibility = 'visible';
    othersText.value = '';
  } else {
    othersText.style.visibility = 'hidden';
  }
});


// 試吓儲住Appointment資料
//currentStylist
// document.querySelector('.dropdown-menu')
//     .addEventListener('click', async function (event) {
//         currentStylist = event.target.innerHTML
//         console.log(currentStylist);
//         //Date reset
//         document.querySelector('#date').innerHTML = getDate(new Date())
//         const res = await fetch("/name/" + currentStylist);
//         const currentStylistSchedule = await res.json();
//         console.log(currentStylistSchedule);
//     });


function checkSubmit() {

  // console.log(currentStylist)

  if (currentStylist == 'empty') {
    // console.log(currentStylist)
    // alert('Please choose Stylist before schedule appointment.');
    alert('提提你，你要揀咗髮型師先可以預約嫁！');
    return false; // prevent submission
  }
  //ServicesType !checked
  let checkAll = document.querySelectorAll('input[name="service_type"]:checked');
  if (checkAll.length == 0) {
    // alert('Please choose your services before submit.');
    alert('咦，你未揀選任何服務book唔到bor！');
    return false; // prevent submission
  }

  //check services period
  let dateSelect = document.getElementById('select-date')
  let timeSelect = document.getElementById('timeChoose')

  if (dateSelect.value == "" && !timeSelect.selected == true) {
    // alert('Please choose your schedule period before submit.');
    alert('唔好唔記得要揀時間呀！');
    return false; // prevent submission
  }
  //Contact Number 8 digit
  let contact = document.getElementById("phone").value.length
  if (contact < 8) {
    // console.log(contact)
    // alert('Invalid phone number,must be 8 digit');
    alert('聯絡電話入錯咗啦！');
    return false; // prevent submission
  }
  // console.log("checkSubmit Checked");
  // document.querySelector('#add-event').submit();
  return true;
};


//submission
document.querySelector('#add-event')
  .addEventListener('submit', async function (event) {
    event.preventDefault();

    // console.log(currentStylist)

    const isCheckOkay = checkSubmit()

    if (!isCheckOkay) {
      return
    }

    const form = event.target;

    let results = [];
    let checkBox = document.querySelectorAll('input[name="service_type"]:checked');
    var first = 0;
    var estimate = 0;

    for (var i = 0; i < checkBox.length; i++) {
      results.push(checkBox[i].id);
      cal = parseFloat(checkBox[i].value);
      first += cal;
      estimate = first / 60;
    } if (form.othersValue.value != "") {
      results.push(form.othersValue.value);
    };
    // console.log(results);
    //testing duration value
    // console.log("this is estimate" + typeof estimate);

    hr = parseInt(estimate);
    // console.log(hr + "hr");

    min = (estimate.toFixed(2).substring(2, 4)) * 60 / 100;
    // console.log(min + "min");

    let tc = document.getElementById('timeChoose');
    let timechooseValue = ([parseInt(tc.options[tc.selectedIndex].value)] + ":00:00");
    // let duration = [parseInt(tc.options[tc.selectedIndex].value) + 1]+":00:00";

    let sdv = form.service_date.value;
    // let thing = str.replace("T", " ");
    let realStart = `${sdv} ${timechooseValue}`;

    // console.log(realStart)

    // const calculate = await fetch("/calculateMoment/"+currentStylist+"/"+ realStart +"/" +duration);
    // const checkDuplications = await res1.json();
    // console.log(checkDuplications)
    let duration = (moment(realStart).add(estimate, 'h')).format('YYYY-MM-DD HH:mm:ss')
    // console.log('this is realStart' +realStart)
    // console.log('this is duration' +duration)

    // let realEnd = parseInt(realStart.substring(11, 13)) + 1;
    // console.log(realEnd);
    // let HH = parseInt(realStart.substring(11, 13)) + hr;
    // let MM = "0" + (String(parseInt(realStart.substring(14, 16)) + parseInt(min))).slice(-2);
    // // console.log(HH);
    // console.log('this is MM' +MM);
    // // let duration = realStart.replace((realStart.substring(11, 13)), realEnd);

    // if (estimate >= 4){
    //   duration = realStart.replace((realStart.substring(11, 13)),parseInt(realStart.substring(11, 13)) + 4);
    // }else {
    //   duration = realStart.replace((realStart.substring(11, 13)), HH).replace((realStart.substring(14, 16)), MM);
    // }
    // console.log(duration);

    const formObject = {};
    formObject['clientName'] = form.clientName.value;
    formObject['phone'] = form.phone.value;
    formObject['email'] = form.email.value;
    formObject['service_type'] = results;
    formObject['service_start'] = realStart;//timechooseValue; //start time 
    formObject['service_end'] = duration; //end time
    formObject['stylist'] = currentStylist;


    // console.log(currentStylist)
    const res1 = await fetch("/checktime/"+currentStylist+"/"+ realStart +"/" +duration);
    const checkDuplications = await res1.json();
    // console.log(checkDuplications)
  
  
    if(checkDuplications[0] != null){
      // console.log('The time is not available, booking failed')
      alert("唔好意思, 你揀既時間已經有人book咗喇");
      return;
      }
    
    const res = await fetch('/events', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(formObject)
    });
    const result = await res.json();

    try {
      if (res.status === 200 && result.success) {
        form.reset();
        document.querySelector('.submission-message').classList.remove('d-none');
        document.querySelector('.dimmer').classList.remove('d-none');
        setTimeout(function () {
          document.querySelector('.submission-message').classList.add('d-none');
          document.querySelector('.dimmer').classList.add('d-none');
        }, 3000);
        // console.log(`current stylish after success: ${currentStylist}`);
       
      }
      removeSchedule()
      addSchedule(day, month, currentStylist);
      // console.log(`current stylish after using addSchedule: ${currentStylist}`);
    } catch (err) {
      console.log({ message: (err) });
    }
  });

  $(document).ready(function () {
    var date_input = $('input[name="service_date"]'); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    date_input.datepicker({
      format: 'yyyy-mm-dd',
      container: container,
      todayHighlight: true,
      autoclose: true,
    })
  })
  