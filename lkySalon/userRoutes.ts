import express ,{Request, Response} from 'express';
import { client } from './app';
import { Users } from './interfaces';
import { checkPassword } from './hash';
// import fetch from 'node-fetch';

export const userRoutes = express.Router();

// local登入嘅step
userRoutes.post('/login', async(req:Request, res:Response) =>{
    try {
        
    
        const result = await client.query(/*sql*/`SELECT * from Users WHERE username = $1`,
            [req.body.username]);  

        const user: Users[] = result.rows;
        const found = user[0];
        console.log(result.rows);
       

        if (found && await checkPassword(req.body.password, found.password)) {
            if (req.session) {
                // 登入，其實就係儲咗user 係session
                req.session.user = found;
            }
            res.json({ success: true});
            return;
            
            // your turn

            // Assign found to req.session.user to represent successful login
        } else {
            res.status(401).json({ success: false, message: "Incorrect Username/Password" });
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Login Failed" })
    }

});

userRoutes.get('/logout', function (req, res) {
    if (req.session) {
        // 登出，就係session 剷走user
        delete req.session.user;
    }
    res.redirect('/form.html')
})


userRoutes.get('/current-user', function (req, res) {
    if (req.session && req.session.user) {
        res.json({login:true,username:req.session.user.username});
    } else {
        res.json({ login: false })
    }
})

