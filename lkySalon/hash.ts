import bcrypt from 'bcryptjs';

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};


export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword); 
    // plainPassword = tecky

    return match;
}

// // 同一個password 每一次hash ，都因為salt 唔同，而得到唔同結果
// hashPassword('tecky')
//     .then(console.log);
// hashPassword('tecky')
//     .then(console.log);

// checkPassword('tecky','$2a$10$a0bXLs.K9LpccDsWW5SRduz8bBMMX/YOZr6EVc0fYunQNgSBHJWyG')
//     .then(console.log);
// checkPassword('tecky','$2a$10$q/Amt1HdPLtUQKjJg30Bo.fr08hgYLfDWjSUf4TwZIsjb4W0BcsE6')
//     .then(console.log);
