import express from 'express';
import { client } from './app';
import { Clients } from './interfaces';
import { isLoggedInAPI } from './guards';
import moment from "moment";

export const customerRoutes = express.Router();
customerRoutes.get('/booking', async function (req, res) {
    const result = await client.query(/*sql*/`select * from clients where "stylist" = $1`,
        ["Karl"]);
    const booking: Clients[] = result.rows;
    //  呢個位你想對return 出嚟嘅memos 做咩都得
    res.json(booking);
});

//get bookings of selected stylist
customerRoutes.get('/name/:name', async function (req, res) {
    // customerRoutes.get('/chooseStylist', async function (req, res) { by YIN
    try {
        let name = req.params.name
        console.log(name)
        console.log(name.substring(0, 1).toUpperCase() + name.substring(1))
        const result = await client.query(/*sql*/`select * from clients where "stylist" = $1`,
            [name.substring(0, 1).toUpperCase() + name.substring(1)]);
        const getStylistSchedule: Clients[] = result.rows;
        res.json(getStylistSchedule);
        
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: " Failed" })
    }
    
})
// get bookings of selected stylist and check time

customerRoutes.get('/checktime/:name/:start_time/:end_time', async function (req, res) {
    try {
        let name = req.params.name;
        let start_time = req.params.start_time.replace("%20", " ")
        let end_time = req.params.end_time.replace("%20", " ")
        let x = (moment(start_time).add(1, 's')).format('YYYY-MM-DD HH:mm:ss')
        let y = (moment(end_time).subtract(1, 's')).format('YYYY-MM-DD HH:mm:ss')
        console.log('this is name'+name)
     
        console.log(start_time)
        console.log(end_time)
        // const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and service_start between '2020-07-26 12:00:00' and '2020-07-26 15:00:00'`,
        // const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and (service_start between $2 and $3 or service_end between $2 and $3) `,
        const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and ((service_start < $2 and service_end > $2) or (service_start < $3 and service_end > $3))`,
        [(name.substring(0, 1).toUpperCase() + name.substring(1)),x,y]);
        const getStylistSchedule: Clients[] = result.rows;
        console.log("Hello!")
        
        res.json(getStylistSchedule);
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: " Failed" })
    }
})

// customerRoutes.get('/calculateMoment, async function (req, res) {
//     try {
//         let x = (moment(start_time).add(1, 's')).format('YYYY-MM-DD HH:mm:ss')
//         let y = (moment(end_time).subtract(1, 's')).format('YYYY-MM-DD HH:mm:ss')
      
//         console.log(start_time)
//         console.log(end_time)
//         // const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and service_start between '2020-07-26 12:00:00' and '2020-07-26 15:00:00'`,
//         // const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and (service_start between $2 and $3 or service_end between $2 and $3) `,
//         const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and ((service_start < $2 and service_end > $2) or (service_start < $3 and service_end > $3))`,
//         [(name.substring(0, 1).toUpperCase() + name.substring(1)),x,y]);
//         const getStylistSchedule: Clients[] = result.rows;
//         console.log("Hello!")
        
//         res.json(getStylistSchedule);
//     } catch (e) {
//         console.log(e);
//         res.status(500).json({ message: " Failed" })
//     }
// })
// customerRoutes.get('/checktime_2/:name/:start_time/:end_time', async function (req, res) {
//     try {
//         let name = req.params.name;
//         let start_time = req.params.start_time.replace("%20", " ")
//         let end_time = req.params.end_time.replace("%20", " ")
     
//         const result = await client.query(/*sql*/`select * from clients where "stylist" = $1 and ("service_start" = $2 or "service_end" = $3)`, 
//         [(name.substring(0, 1).toUpperCase() + name.substring(1)),end_time,start_time]);
//         const getStylistSchedule: Clients[] = result.rows;
        
//         res.json(getStylistSchedule);
//     } catch (e) {
//         console.log(e);
//         res.status(500).json({ message: " Failed" })
//     }
// })

// 接appointments
customerRoutes.post('/events', async function (req, res) {
    try {
        console.log(req.body);
        await client.query(/*sql */`INSERT INTO Clients (clientName, phone, email, service_type, service_start, service_end, stylist,user_id) VALUES ($1,$2,$3,$4,$5,$6,$7,(SELECT id from Users where username =$8))`,
            [req.body.clientName, req.body.phone, req.body.email, req.body.service_type, req.body.service_start, req.body.service_end, req.body.stylist, req.body.stylist]);
        res.json({ success: true, message: 'new-appointment-received' });
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Fail-appointment" })
    }
});



// customerRoutes.put('/memos/:id',isLoggedInAPI,async function(req,res){
//     const id = parseInt(req.params.id);
//     if(isNaN(id)){
//         res.status(400).json({"message":"id not a number!!!"});
//         return;
//     }
//     await client.query(/* sql */`UPDATE clients set content = $1 WHERE id = $2`,
//             [req.body.editContent,id]);

//     res.json({success:true});
// })


customerRoutes.delete('/memos/:id',isLoggedInAPI,async function(req,res){
    const id = parseInt(req.params.id);
    if(isNaN(id)){
        res.status(400).json({"message":"id not a number!!!"});
        return;
    }
    await client.query('DELETE FROM clients WHERE id = $1',[id]);
    res.json({success:true});
});
